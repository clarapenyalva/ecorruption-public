<!-- Title is no longer necessary as we have the image # ECORRUPTION -->
![alt text1][logo]

You got elected into power. Choose between protecting the planet, succumbing to money and power or pleasing public opinion, all while you try to stay in charge. 

ECORRUPTION is a turn-based game of strategy, planning and judgement. 

[Developed in 2019]

## Requisites
 Unity version: 2018.3.8f1
 

## Credits
*  Jaime Elcarte Fontcuberta
*  Cristian Soler Sierra [[webpage](https://crisosie.github.io/)]
*  Miguel Medina Patón
*  Patricia Ferández-Mariñas Bustamante [[webpage](https://pfernandezmarinas.github.io/)]
*  Clara Peñalva Carbonell [[webpage](https://clarapenyalva.com)] [[GitLab profile](https://gitlab.com/clarapenyalva)]
*  José María Pizana García  [[webpage](https://jmpizana.com)] [[GitLab profile](https://gitlab.com/jmpizanagarcia)]


<!-- References section -->
[logo]: ECORRUPTION_Unity/Assets/Art/Images/Other/ECORRUPTION_WhatIsIt%20(1).png

