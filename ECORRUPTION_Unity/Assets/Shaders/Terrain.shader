﻿Shader "Custom/terrainIndex" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("terrainIndex Texture Array", 2DArray) = "white" {}
		_GridTex ("Grid Texture", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Specular ("Specular", Color) = (0.2, 0.2, 0.2)
		_BackgroundColor ("Background Color", Color) = (0,0,0)
		_HighlightEmissionColor ("Highlight Emission Color", Color) = (0,0,0)
		//[Toggle(SHOW_MAP_DATA)]_ShowMapData ("Show Map Data", Float) = 0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf StandardSpecular fullforwardshadows vertex:vert
		#pragma target 3.5

		#pragma multi_compile _ GRID_ON
		#pragma multi_compile _ HEX_MAP_EDIT_MODE

		#pragma shader_feature SHOW_MAP_DATA

		#include "HexMetrics.cginc"
		#include "HexCellData.cginc"

		UNITY_DECLARE_TEX2DARRAY(_MainTex);

		sampler2D _GridTex;

		half _Glossiness;
		fixed3 _Specular;
		fixed4 _Color;
		half3 _BackgroundColor;
		half3 _HighlightEmissionColor;

		struct Input {
			float4 color : COLOR;
			float3 worldPos;
			float3 terrainIndex;
			float blockedCellFactor;
			float highlightedFactor;
			//float4 visibility;
		};

		void vert (inout appdata_full v, out Input data) {
			UNITY_INITIALIZE_OUTPUT(Input, data);

			float4 cell0Data = GetCellData(v, 0);
			float4 cell1Data = GetCellData(v, 1);
			float4 cell2Data = GetCellData(v, 2);

			//TerrainIndex
			data.terrainIndex.x = cell0Data.w * 255;
			data.terrainIndex.y = cell1Data.w * 255;
			data.terrainIndex.z = cell2Data.w * 255;

			//Blocked
			data.blockedCellFactor =
				cell0Data.y * v.color.x
				+ cell1Data.y * v.color.y
				+ cell2Data.y * v.color.z
				;

			//Highlighted
			data.highlightedFactor = 
				cell0Data.x * v.color.x
				+ cell1Data.x * v.color.y
				+ cell2Data.x * v.color.z
				;
			//Visibility factor
			/*data.visibility.x = cell0Data.x;
			data.visibility.y = cell1Data.x;
			data.visibility.z = cell2Data.x;
			data.visibility.xyz = lerp(0.25, 1, data.visibility.xyz);
			data.visibility.w =
				cell0Data.y * v.color.x 
				+ cell1Data.y * v.color.y 
				+ cell2Data.y * v.color.z
				;*/			
		}

		float4 GetTerrainColor (Input IN, int index) {
			float3 uvw = float3(
				IN.worldPos.xz * (2 * TILING_SCALE),
				IN.terrainIndex[index]
			);
			float4 c = UNITY_SAMPLE_TEX2DARRAY(_MainTex, uvw);
			//return c * (IN.color[index] * IN.visibility[index]);
			return c * IN.color[index];
		}

		void surf (Input IN, inout SurfaceOutputStandardSpecular o) {
			fixed4 c =
				GetTerrainColor(IN, 0) +
				GetTerrainColor(IN, 1) +
				GetTerrainColor(IN, 2);

			fixed4 grid = 1;
			#if defined(GRID_ON)
				float2 gridUV = IN.worldPos.xz;
				gridUV.x *= 1 / (4 * 8.66025404);
				gridUV.y *= 1 / (2 * 15.0);
				grid = tex2D(_GridTex, gridUV);
			#endif

			//float explored = IN.visibility.w;
			/*float explored = 1.0;
			o.Albedo = c.rgb * grid * _Color * explored;*/

			/*#if defined(SHOW_MAP_DATA)
				o.Albedo = IN.mapData * grid;
			#endif*/

			o.Albedo = c.rgb * grid * _Color;

			//JMP: Grey scale. Recommendable applied last to Albedo. This color could be:
			// float grey = dot(greyVector, o.Albedo);
			float grey = 0.21 * o.Albedo.r + 0.71 * o.Albedo.g + 0.07 * o.Albedo.b;
			o.Albedo = o.Albedo * (1.0-IN.blockedCellFactor)
				+ float3(grey, grey, grey) * (IN.blockedCellFactor);
			o.Specular = _Specular * (1.0- IN.blockedCellFactor);
			o.Smoothness = _Glossiness;
			o.Occlusion = IN.blockedCellFactor;
			o.Emission = _HighlightEmissionColor * IN.highlightedFactor;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}