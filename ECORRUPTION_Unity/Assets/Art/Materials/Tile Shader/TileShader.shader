﻿Shader "Custom/Grayscale Tile Shader" {
	Properties{
	  _MainTex("Texture", 2D) = "white" {}
	  _Blocked("Blocked", float) = 0.0
	}
	SubShader{
		Tags { "RenderType" = "Opaque" }
		CGPROGRAM
		#pragma surface surf Lambert
		struct Input {
			float2 uv_MainTex;
		};
		sampler2D _MainTex;
		float _Blocked;
		void surf(Input IN, inout SurfaceOutput o) {
			if (_Blocked == 0.0) {
				o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
			}
			else {
				half4 c = tex2D(_MainTex, IN.uv_MainTex);
				o.Albedo = dot(c.rgb, float3(0.3, 0.59, 0.11));
				o.Alpha = c.a;
			}
		}
		ENDCG
	}

	Fallback "Diffuse"
}