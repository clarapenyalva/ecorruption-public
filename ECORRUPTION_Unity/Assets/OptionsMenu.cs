﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour
{
    public static OptionsMenu instance;

    public Slider masterVolume, musicVolume, ambientVolume, fxVolume;

    public void Awake()
    {
        if (instance != null && instance != this) Destroy(gameObject);
        else instance = this;

        InitializeSoundLevels();
    }

    public void InitializeSoundLevels()
    {
        masterVolume.value = PlayerPrefs.GetFloat("MasterVolume");
        musicVolume.value = PlayerPrefs.GetFloat("MusicVolume");
        ambientVolume.value = PlayerPrefs.GetFloat("AmbientVolume");
        fxVolume.value = PlayerPrefs.GetFloat("FXVolume");
    }
}
