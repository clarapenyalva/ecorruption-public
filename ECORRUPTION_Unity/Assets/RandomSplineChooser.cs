﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECUtils;

[RequireComponent(typeof(SplineWalker))]
public class RandomSplineChooser : MonoBehaviour
{
    public List<BezierSpline> possibleSplines;

    private void Awake()
    {
        SplineWalker walker = GetComponent<SplineWalker>();
        walker.spline = possibleSplines.GetRandom();
        walker.progress = Random.Range(0f, 1f);
        float scaleRange = 0.1f;
        walker.transform.localScale *= Random.Range(1f - scaleRange, 1f + scaleRange);
    }

}
