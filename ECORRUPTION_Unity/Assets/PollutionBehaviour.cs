﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PollutionBehaviour : MonoBehaviour
{
    public PostProcessVolume contamination;

    public HexMapCamera hexMapCam;

    private float highSaturation = 0.0f;
    private float lowSaturation = -40.0f;
    private float highVignete = 0.3f;
    private float lowVignete = 0.121f;
    private float highFogIntensityMin = 0.000075f;
    private float highFogIntensityMax = 0.00015f;
    private float lowFogIntensity = 0.0f;
    private Color lightFog = new Color(91.0f, 80.4f, 67.1f);
    private Color lightMediumFog = new Color(82.7f, 67.1f, 56.1f);
    private Color darkMediumFog = new Color(63.9f, 46.7f, 39.2f);
    private Color darkFog = new Color(37.6f, 29.8f, 23.9f);

    //public float testEco;

    // Start is called before the first frame update
    void Awake()
    {
        hexMapCam.minFog = hexMapCam.maxFog = 0.0f;
        //testEco = 50.0f;
    }

    //private void Update()
    //{
    //    ApplyPollution(testEco);
    //}

    public void ApplyPollution(float ecoLevel)
    {
        // Viñeteado (Varia entre 0.161 y 0.3. Inversamente proporcional)
        contamination.profile.GetSetting<Vignette>().intensity.value = ecoLevel * (lowVignete - highVignete) / 100.0f + highVignete;

        // Saturacion (Varia entre -40 y 0)
        contamination.profile.GetSetting<ColorGrading>().saturation.value = ecoLevel * (highSaturation-lowSaturation) / 100.0f + lowSaturation;

        // Densidad niebla (Varia entre 0 y 0.002 si el nivel de ecologia es inferior a 50. Inversamente proporcional)
        if (ecoLevel < 50)
        {
            hexMapCam.minFog = ecoLevel * (lowFogIntensity - highFogIntensityMin) / 50 + highFogIntensityMin;
            hexMapCam.maxFog = ecoLevel * (lowFogIntensity - highFogIntensityMax) / 50 + highFogIntensityMax;
        } else
        {
            hexMapCam.minFog = hexMapCam.maxFog = 0.0f;
        }

        // Color niebla (varia entre 4 tonalidades de marron)
        if (ecoLevel < 20.0f)
            RenderSettings.fogColor = darkFog;
        else if (ecoLevel < 30.0f)
            RenderSettings.fogColor = Color.Lerp(darkFog, darkMediumFog, 0.1f * ecoLevel - 1.0f);
        else if (ecoLevel < 40.0f)
            RenderSettings.fogColor = Color.Lerp(darkMediumFog, lightMediumFog, 0.1f * ecoLevel - 1.0f);
        else if (ecoLevel < 50.0f)
            RenderSettings.fogColor = Color.Lerp(lightMediumFog, lightFog, 0.1f * ecoLevel - 1.0f);
        else
            RenderSettings.fogColor = lightFog;

    }
}
