﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class AdvanceOilTex : MonoBehaviour
{
    public float speed = 1f;

    Material mat;
    float offset = 0f;


    // Start is called before the first frame update
    void Start()
    {
        mat = GetComponent<MeshRenderer>().material;
        offset = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        mat.mainTextureOffset += Vector2.up * Time.deltaTime * speed;
    }
}
