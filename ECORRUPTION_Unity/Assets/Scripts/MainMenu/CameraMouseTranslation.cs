﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraMouseTranslation : MonoBehaviour
{

    public Transform center;
    public float maxAngle = 12.5f, maxMov = 0.05f;//How many degrees it can rotate to each side
    public bool linealCombination = false;

    public bool invertedX = false, invertedY = false;

    Camera cam;
    float vertAng, horzAng, radius, centerVert, centerHorz;
    Vector3 initialPos;

    private void Awake()
    {
        radius = (transform.position - center.position).magnitude;
        maxAngle *= Mathf.Deg2Rad;

        centerVert = Mathf.Acos(transform.position.y / radius);
        centerHorz = Mathf.Atan2(transform.position.z, transform.position.x);

        initialPos = transform.position;

    }

    private void Start()
    {
        cam = GetComponent<Camera>();
    }

    void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        if (!cam.pixelRect.Contains(mousePos)) return/*mousePos = Vector3.zero*/ ;

        int width = cam.pixelWidth,
            height = cam.pixelHeight;

        //We map spherical coordinates to mouseposition

        float relativeMousePosY = mousePos.y / height;
        float relativeMousePosX = mousePos.x / width;

        if (invertedY) relativeMousePosY = 1 - relativeMousePosY;
        if (invertedX) relativeMousePosX = 1 - relativeMousePosX;

        vertAng = centerVert + Mathf.Lerp(maxAngle, -maxAngle, relativeMousePosY);
        horzAng = centerHorz + Mathf.Lerp(-maxAngle, maxAngle, relativeMousePosX);

        //Debug.Log(vertAng + " " + horzAng);
        
        Vector3 newPosition = new Vector3(
            radius * Mathf.Sin(vertAng) * Mathf.Cos(horzAng),
            radius * Mathf.Cos(vertAng),
            radius * Mathf.Sin(vertAng) * Mathf.Sin(horzAng)
            );



        transform.position = newPosition;

        //transform.position -= Vector3.forward * 20;

        transform.LookAt(center);

        if (linealCombination)
        {
            Vector3 positionOffset = new Vector3(
                 Mathf.Lerp(-maxMov, maxMov, mousePos.x / width),
                 Mathf.Lerp(-maxMov, maxMov, mousePos.y / height),
                 0
                );

           // newPosition += positionOffset;
            transform.position += positionOffset;
        }
    }
}
