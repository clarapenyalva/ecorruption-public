﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using UnityEngine.SceneManagement;
using System.IO;

public class MainMenuManager : MonoBehaviour
{

    public MixerControlls mixerControls;

    public RectTransform continueGameButton;

    private void Start()
    {
        float masterVolume = PlayerPrefs.GetFloat("MasterVolume");
        if (masterVolume < 0.0001f)
            masterVolume = 1.0f;
        mixerControls.SetMasterVolume(masterVolume);

        float musicVolume = PlayerPrefs.GetFloat("MusicVolume");
        if (musicVolume < 0.0001f)
            musicVolume = 1.0f;
        mixerControls.SetMusicVolume(musicVolume);

        float ambientVolume = PlayerPrefs.GetFloat("AmbientVolume");
        if (ambientVolume < 0.0001f)
            ambientVolume = 1.0f;
        mixerControls.SetAmbientVolume(ambientVolume);

        float fxVolume = PlayerPrefs.GetFloat("FXVolume");
        if (fxVolume < 0.0001f)
            fxVolume = 1.0f;
        mixerControls.SetFXVolume(fxVolume);

        bool hasSavedGame = PlayerPrefs.GetInt("HasSavedGame", 0) == 1;
        hasSavedGame = hasSavedGame && File.Exists(Application.persistentDataPath + "/NEWJSON.json");
        if (!hasSavedGame)
        {
            continueGameButton.gameObject.SetActive(false);
        }
    }

    public void NewGame()
    {
        PlayerPrefs.SetInt("LoadGame", 0);
        SceneManager.LoadScene(1);
    }

    public void ContinueGame()
    {
        PlayerPrefs.SetInt("LoadGame", 1);
        SceneManager.LoadScene(1);
    }

    public void CloseGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
            #else
        Application.Quit();
#endif
    }

}
