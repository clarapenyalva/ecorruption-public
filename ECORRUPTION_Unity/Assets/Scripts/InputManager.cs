﻿using UnityEngine;
using UnityEngine.EventSystems;

public class InputManager : MonoBehaviour
{
    public static InputManager instance;

    [Header("Camera")]
    [HideInInspector] public bool lockedCamera = false;

    [Range(0f, 0.5f)]
    [Tooltip("Minimum screen coordinates expressed in [0,1] that the mouse has to be in to move.")]
    public float minScreenMovementThreshold = .25f;

    [Range(0.5f, 1f)]
    [Tooltip("Maximum screen coordinates expressed in [0,1] that the mouse has to be in to move.")]
    public float maxScreenMovementThreshold = .75f;

    [Range(0f, 100f)] public float mouseMovSpeed = 5f;
    [Range(0f, 1f)] public float mouseCamRotSensitivity = 0.5f;

    public bool moveCameraWEdges = true;

    [Range(0.05f, 2f)]
    public float zoomSensitivity = .8f;

    //JMP: if the camera is on the top half, reverse movement. Values 1 and -1
    float initialCamDragMultiplier = 1;
    Vector2 prevMouseScreenPos;
    Vector2 initialDragScreenPos = -Vector2.one;
    Vector2 currentDragScreenPosDifference = -Vector2.one;

    ECView view;
    HexCell selectedCell;

    private void Awake()
    {
        if (instance != null && instance != this) Destroy(gameObject);
        else instance = this;
    }

    private void Start()
    {
        view = ECView.instance;
    }

    private void Update()
    {
        MapSelection();

        CameraControl();

        //JMP: si le damos al escape debería portarse como si clicásemos a
        // una casilla nula
        if (Input.GetAxisRaw("Cancel") != 0)
            view.OnClickOnMap(null);


        prevMouseScreenPos = Input.mousePosition;
    }

    private void MapSelection()
    {
        //JMP: si el ratón NO está encima de un objeto de canvas interactuable
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            HexCell prevCell = selectedCell;

            UpdateCurrentCell();

            view.OnHoverOnMap(selectedCell ? selectedCell.ecCellV : null);

            if (Input.GetMouseButtonUp(0))
            {
                view.OnClickOnMap(selectedCell ? selectedCell.ecCellV : null);
            }
            //else if (Input.GetMouseButtonDown(0))
            //          etc...

        }
    }

    private bool UpdateCurrentCell()
    {
        HexCell cell = view.GetCell(Camera.main.ScreenPointToRay(Input.mousePosition));
        if (cell != selectedCell)
        {
            selectedCell = cell;
            return true;
        }
        return false;
    }

    private void CameraControl() //Done by JMP. Ask me or burn me 🌲
    {
        //ZOOM CONTROL
        float zoom = Input.GetAxis("Mouse ScrollWheel") * zoomSensitivity;
        if (zoom != 0) view.camRig.AdjustZoom(zoom);

        if (lockedCamera) return;

        float horizontal = 0, vertical = 0;

        //KEYBOARD CONTROLS
        horizontal += Input.GetAxis("Horizontal");
        vertical += Input.GetAxis("Vertical");

        if (horizontal != 0 || vertical != 0)
        {
            //TRANSLATION WITH KEYBOARD
            view.camRig.ControlledMovement(horizontal, vertical);
        }
        else
        {
            //MOUSE CONTROLS


            //JMP: esto esta en obras, no tocar por ahora
            //if (Input.GetMouseButton(1))
            //{
            //    //TRANSLATION WITH RIGHT BUTTON

            //    //Vers1: translation just from mouse differential between frames
            //    //Vector2 displ = 
            //    //    new Vector2(Input.mousePosition.x, Input.mousePosition.y) 
            //    //    - prevMouseScreenPos;
            //    //displ *= mouseMovSpeed;

            //    //Vers2: translation having an anchored position when the mouse was first selected

            //    if (initialDragScreenPos.x < 0)
            //    {
            //        initialDragScreenPos = Input.mousePosition;
            //        currentDragScreenPosDifference = Vector2.zero;
            //    }

            //    Vector2 newDragScreenPosDifference = (Vector2)Input.mousePosition - initialDragScreenPos;

            //    Vector2 displ = newDragScreenPosDifference - currentDragScreenPosDifference;

            //    displ = displ.normalized *  mouseMovSpeed;

            //    Debug.Log(displ.magnitude);

            //    horizontal = -displ.x;
            //    vertical = -displ.y;

            //    currentDragScreenPosDifference = newDragScreenPosDifference;
            //}
            //if (Input.GetMouseButtonUp(1))
            //{
            //    initialDragScreenPos = -Vector2.one;
            //}
                        
            if (moveCameraWEdges && horizontal == 0 && vertical == 0)
            {
                //TRANSLATION WITH SCREEN BORDERS
                Vector2 normalizedMousePos = Input.mousePosition;
                normalizedMousePos = new Vector2(normalizedMousePos.x / Screen.width, normalizedMousePos.y / Screen.height);

                if (normalizedMousePos.x < minScreenMovementThreshold)
                {
                    horizontal -= 1;
                }
                else if (normalizedMousePos.x > maxScreenMovementThreshold)
                {
                    horizontal += 1;
                }

                if (normalizedMousePos.y < minScreenMovementThreshold)
                {
                    vertical -= 1;
                }
                else if (normalizedMousePos.y > maxScreenMovementThreshold)
                {
                    vertical += 1;
                }
            }

            view.camRig.ControlledMovement(horizontal, vertical);

            //ROTATION WITH CENTER BUTTON

            //JMP: Commented for now, this control is weird
            //if (Input.GetMouseButtonDown(1))
            //{
            //    //JMP: if the camera is on the top half, reverse movement
            //    initialCamDragMultiplier = Input.mousePosition.y / Screen.height > 0.5 ? -1 : 1;
            //}

            if (Input.GetMouseButton(2))
            {
                float mouseMovHoriz = Input.mousePosition.x - prevMouseScreenPos.x;
                //Debug.Log(mouseMovHoriz * mouseCamRotSensitivity);
                view.camRig.AdjustRotation(initialCamDragMultiplier * mouseMovHoriz * mouseCamRotSensitivity);
            }
        }

        //ROTATION CONTROL
        float rotationDelta = Input.GetAxis("Rotation");
        if (rotationDelta != 0f)
        {
            view.camRig.AdjustRotation(rotationDelta);
        }

    }
}
