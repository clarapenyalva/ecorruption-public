﻿using UnityEngine;

[CreateAssetMenu(menuName ="Hex Metrics")]
public class HexMetrics : ScriptableObject
{
    [Tooltip("How mucho time takes for shader parameter transitions to complete.")]
    public float visualTransitionSeconds = 1f;

    public bool generateFeatures = false;

    public float outerToInner = 0.866025404f;

    public float InnerToOuter { get { return 1f / outerToInner; } }

    public float outerRadius = 10f;

    public float InnerRadius { get { return outerRadius * outerToInner; } }

    public float InnerDiameter { get { return InnerRadius * 2f; } }

    public float solidFactor = 0.8f;

    public float blendFactor { get { return 1f - solidFactor; } }

    public float waterFactor = 0.6f;

    public float waterBlendFactor { get { return 1f - waterFactor; } }

    public float elevationStep = 1f;

    public int terracesPerSlope = 2;

    public int terraceSteps { get { return terracesPerSlope * 2 + 1; } }

    public float horizontalTerraceStepSize { get { return 1f / terraceSteps; } }

    public float verticalTerraceStepSize { get { return 1f / (terracesPerSlope + 1); } }

    public float cellPerturbStrength = 4f;

    public float elevationPerturbStrength = 1.5f;

    public float streamBedElevationOffset = -1.75f;

    public float waterElevationOffset = -0.5f;

    public float wallHeight = 4f;

    public float wallYOffset = -1f;

    public float wallThickness = 0.75f;

    public float wallElevationOffset { get { return verticalTerraceStepSize; } }

    public float wallTowerThreshold = 0.5f;

    public float bridgeDesignLength = 7f;

    public float noiseScale = 0.003f;

    public int chunkSizeX = 5, chunkSizeZ = 5;

    public int hashGridSize = 256;

    public float hashGridScale = 0.25f;

    static HexHash[] hashGrid;

    Vector3[] Corners
    {
        get
        {
            return new Vector3[] {
                new Vector3(0f, 0f, outerRadius),
                new Vector3(InnerRadius, 0f, 0.5f * outerRadius),
                new Vector3(InnerRadius, 0f, -0.5f * outerRadius),
                new Vector3(0f, 0f, -outerRadius),
                new Vector3(-InnerRadius, 0f, -0.5f * outerRadius),
                new Vector3(-InnerRadius, 0f, 0.5f * outerRadius),
                new Vector3(0f, 0f, outerRadius)
            };
        }
    }

    float[][] featureThresholds = {
        new float[] {0.0f, 0.0f, 0.4f},
        new float[] {0.0f, 0.4f, 0.6f},
        new float[] {0.4f, 0.6f, 0.8f}
    };

    public Texture2D noiseSource;

    public Vector4 SampleNoise(Vector3 position)
    {
        Vector4 sample = noiseSource.GetPixelBilinear(
            position.x * noiseScale,
            position.z * noiseScale
        );

        if (Wrapping && position.x < InnerDiameter * 1.5f)
        {
            Vector4 sample2 = noiseSource.GetPixelBilinear(
                (position.x + wrapSize * InnerDiameter) * noiseScale,
                position.z * noiseScale
            );
            sample = Vector4.Lerp(
                sample2, sample, position.x * (1f / InnerDiameter) - 0.5f
            );
        }

        return sample;
    }

    public int wrapSize;

    public bool Wrapping
    {
        get
        {
            return wrapSize > 0;
        }
    }

    public void InitializeHashGrid(int seed)
    {
        hashGrid = new HexHash[hashGridSize * hashGridSize];
        Random.State currentState = Random.state;
        Random.InitState(seed);
        for (int i = 0; i < hashGrid.Length; i++)
        {
            hashGrid[i] = HexHash.Create();
        }
        Random.state = currentState;
    }

    public HexHash SampleHashGrid(Vector3 position)
    {
        int x = (int)(position.x * hashGridScale) % hashGridSize;
        if (x < 0)
        {
            x += hashGridSize;
        }
        int z = (int)(position.z * hashGridScale) % hashGridSize;
        if (z < 0)
        {
            z += hashGridSize;
        }
        return hashGrid[x + z * hashGridSize];
    }

    public float[] GetFeatureThresholds(int level)
    {
        return featureThresholds[level];
    }

    public Vector3 GetFirstCorner(HexDirection direction)
    {
        return Corners[(int)direction];
    }

    public Vector3 GetSecondCorner(HexDirection direction)
    {
        return Corners[(int)direction + 1];
    }

    public Vector3 GetFirstSolidCorner(HexDirection direction)
    {
        return Corners[(int)direction] * solidFactor;
    }

    public Vector3 GetSecondSolidCorner(HexDirection direction)
    {
        return Corners[(int)direction + 1] * solidFactor;
    }

    public Vector3 GetSolidEdgeMiddle(HexDirection direction)
    {
        return
            (Corners[(int)direction] + Corners[(int)direction + 1]) *
            (0.5f * solidFactor);
    }

    public Vector3 GetFirstWaterCorner(HexDirection direction)
    {
        return Corners[(int)direction] * waterFactor;
    }

    public Vector3 GetSecondWaterCorner(HexDirection direction)
    {
        return Corners[(int)direction + 1] * waterFactor;
    }

    public Vector3 GetBridge(HexDirection direction)
    {
        return (Corners[(int)direction] + Corners[(int)direction + 1]) *
            blendFactor;
    }

    public Vector3 GetWaterBridge(HexDirection direction)
    {
        return (Corners[(int)direction] + Corners[(int)direction + 1]) *
            waterBlendFactor;
    }

    public Vector3 TerraceLerp(Vector3 a, Vector3 b, int step)
    {
        float h = step * horizontalTerraceStepSize;
        a.x += (b.x - a.x) * h;
        a.z += (b.z - a.z) * h;
        float v = ((step + 1) / 2) * verticalTerraceStepSize;
        a.y += (b.y - a.y) * v;
        return a;
    }

    public Color TerraceLerp(Color a, Color b, int step)
    {
        float h = step * horizontalTerraceStepSize;
        return Color.Lerp(a, b, h);
    }

    public Vector3 WallLerp(Vector3 near, Vector3 far)
    {
        near.x += (far.x - near.x) * 0.5f;
        near.z += (far.z - near.z) * 0.5f;
        float v =
            near.y < far.y ? wallElevationOffset : (1f - wallElevationOffset);
        near.y += (far.y - near.y) * v + wallYOffset;
        return near;
    }

    public Vector3 WallThicknessOffset(Vector3 near, Vector3 far)
    {
        Vector3 offset;
        offset.x = far.x - near.x;
        offset.y = 0f;
        offset.z = far.z - near.z;
        return offset.normalized * (wallThickness * 0.5f);
    }

    public HexEdgeType GetEdgeType(int elevation1, int elevation2)
    {
        if (elevation1 == elevation2)
        {
            return HexEdgeType.Flat;
        }
        int delta = elevation2 - elevation1;
        if (delta == 1 || delta == -1)
        {
            return HexEdgeType.Slope;
        }
        return HexEdgeType.Cliff;
    }

    public Vector3 Perturb(Vector3 position)
    {
        Vector4 sample = SampleNoise(position);
        position.x += (sample.x * 2f - 1f) * cellPerturbStrength;
        position.z += (sample.z * 2f - 1f) * cellPerturbStrength;
        return position;
    }
}