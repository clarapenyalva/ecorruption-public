﻿using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class HexCell : MonoBehaviour
{

    public HexCoordinates coordinates;

    public RectTransform uiRect;

    public HexGridChunk chunk;

    public int Index { get; set; }

    public int ColumnIndex { get; set; }

    public ECCellV ecCellV;

    public int Elevation
    {
        get
        {
            return elevation;
        }
        set
        {
            if (elevation == value)
            {
                return;
            }
            int originalViewElevation = ViewElevation;
            elevation = value;
            if (ViewElevation != originalViewElevation)
            {
                ShaderData.ViewElevationChanged();
            }
            RefreshPosition();
            ValidateRivers();

            for (int i = 0; i < roads.Length; i++)
            {
                if (roads[i] && GetElevationDifference((HexDirection)i) > 1)
                {
                    SetRoad(i, false);
                }
            }

            Refresh();
        }
    }

    public int WaterLevel
    {
        get
        {
            return waterLevel;
        }
        set
        {
            if (waterLevel == value)
            {
                return;
            }
            int originalViewElevation = ViewElevation;
            waterLevel = value;
            if (ViewElevation != originalViewElevation)
            {
                ShaderData.ViewElevationChanged();
            }
            ValidateRivers();
            Refresh();
        }
    }

    public int ViewElevation
    {
        get
        {
            return elevation >= waterLevel ? elevation : waterLevel;
        }
    }

    public bool IsUnderwater
    {
        get
        {
            return waterLevel > elevation;
        }
    }

    public bool HasIncomingRiver
    {
        get
        {
            return hasIncomingRiver;
        }
    }

    public bool HasOutgoingRiver
    {
        get
        {
            return hasOutgoingRiver;
        }
    }

    public bool HasRiver
    {
        get
        {
            return hasIncomingRiver || hasOutgoingRiver;
        }
    }

    public bool HasRiverBeginOrEnd
    {
        get
        {
            return hasIncomingRiver != hasOutgoingRiver;
        }
    }

    public HexDirection RiverBeginOrEndDirection
    {
        get
        {
            return hasIncomingRiver ? incomingRiver : outgoingRiver;
        }
    }

    public bool HasRoads
    {
        get
        {
            for (int i = 0; i < roads.Length; i++)
            {
                if (roads[i])
                {
                    return true;
                }
            }
            return false;
        }
    }

    public HexDirection IncomingRiver
    {
        get
        {
            return incomingRiver;
        }
    }

    public HexDirection OutgoingRiver
    {
        get
        {
            return outgoingRiver;
        }
    }

    public Vector3 Position
    {
        get
        {
            return transform.localPosition;
        }
    }

    public float StreamBedY
    {
        get
        {
            return
                (elevation + ECModel.instance.hexMetrics.streamBedElevationOffset) *
                ECModel.instance.hexMetrics.elevationStep;
        }
    }

    public float RiverSurfaceY
    {
        get
        {
            return
                (elevation + ECModel.instance.hexMetrics.waterElevationOffset) *
                ECModel.instance.hexMetrics.elevationStep;
        }
    }

    public float WaterSurfaceY
    {
        get
        {
            return
                (waterLevel + ECModel.instance.hexMetrics.waterElevationOffset) *
                ECModel.instance.hexMetrics.elevationStep;
        }
    }

    public int UrbanLevel
    {
        get
        {
            return urbanLevel;
        }
        set
        {
            if (urbanLevel != value)
            {
                urbanLevel = value;
                RefreshSelfOnly();
            }
        }
    }

    public int FarmLevel
    {
        get
        {
            return farmLevel;
        }
        set
        {
            if (farmLevel != value)
            {
                farmLevel = value;
                RefreshSelfOnly();
            }
        }
    }

    public int PlantLevel
    {
        get
        {
            return plantLevel;
        }
        set
        {
            if (plantLevel != value)
            {
                plantLevel = value;
                RefreshSelfOnly();
            }
        }
    }

    public int SpecialIndex
    {
        get
        {
            return specialIndex;
        }
        set
        {
            if (specialIndex != value && !HasRiver)
            {
                specialIndex = value;
                RemoveRoads();
                RefreshSelfOnly();
            }
        }
    }

    public bool IsSpecial
    {
        get
        {
            return specialIndex > 0;
        }
    }

    public bool Walled
    {
        get
        {
            return walled;
        }
        set
        {
            if (walled != value)
            {
                walled = value;
                Refresh();
            }
        }
    }

    public int TerrainTypeIndex
    {
        get
        {
            return terrainTypeIndex;
        }
        set
        {
            if (terrainTypeIndex != value)
            {
                terrainTypeIndex = value;
                ShaderData.RefreshTerrain(this);
            }
        }
    }

    //public bool IsVisible {
    //	get {
    //		return visibility > 0 && Explorable;
    //	}
    //}



    public bool IsExplored
    {
        get
        {
            return explored && Explorable;
        }
        private set
        {
            explored = value;
        }
    }

    public bool Explorable { get; set; }

    public int Distance
    {
        get
        {
            return distance;
        }
        set
        {
            distance = value;
        }
    }

    //public HexUnit Unit { get; set; }

    public HexCell PathFrom { get; set; }

    public int SearchHeuristic { get; set; }

    public int SearchPriority
    {
        get
        {
            return distance + SearchHeuristic;
        }
    }

    public int SearchPhase { get; set; }

    public HexCell NextWithSamePriority { get; set; }

    /// <summary>
    /// JMP: Shared Shader Data for all cells. THIS IS A REFERENCE
    /// </summary>
    public HexCellShaderData ShaderData { get; set; }

    int terrainTypeIndex;

    int elevation = int.MinValue;
    int waterLevel;

    int urbanLevel, farmLevel, plantLevel;

    int specialIndex;

    int distance;

    //int visibility;

    float blockedFactor = 0; //JMP

    float highlightedFactor = 0; //JMP

    bool explored;

    bool walled;

    bool hasIncomingRiver, hasOutgoingRiver;
    HexDirection incomingRiver, outgoingRiver;

    [SerializeField]
    public HexCell[] neighbors;

    [SerializeField]
    bool[] roads;

    //public void IncreaseVisibility()
    //{
    //    visibility += 1;
    //    if (visibility == 1)
    //    {
    //        IsExplored = true;
    //        ShaderData.RefreshVisibility(this);
    //    }
    //}

    //public void DecreaseVisibility()
    //{
    //    visibility -= 1;
    //    if (visibility == 0)
    //    {
    //        ShaderData.RefreshVisibility(this);
    //    }
    //}

    //public void ResetVisibility()
    //{
    //    if (visibility > 0)
    //    {
    //        visibility = 0;
    //        ShaderData.RefreshVisibility(this);
    //    }
    //}

    public void RefreshShaders()
    {
        ShaderData.RefreshAll(this);
    }

    public float BlockedFactor
    {
        get { return blockedFactor; }
        set
        {
            value = Mathf.Clamp01(value);
            blockedFactor = value;
            //Debug.Log("Refresing new blocked factor: " + blockedFactor);
            ShaderData.RefreshBlocked(this);
        }
    }

    public float HighlightedFactor
    {
        get { return highlightedFactor; }
        set
        {
            value = Mathf.Clamp01(value);
            highlightedFactor = value;
            ShaderData.RefreshHighlight(this);
        }
    }

    public HexCell GetNeighbor(HexDirection direction)
    {
        return neighbors[(int)direction];
    }

    public void SetNeighbor(HexDirection direction, HexCell cell)
    {
        neighbors[(int)direction] = cell;
        cell.neighbors[(int)direction.Opposite()] = this;
    }

    public HexEdgeType GetEdgeType(HexDirection direction)
    {
        return ECModel.instance.hexMetrics.GetEdgeType(
            elevation, neighbors[(int)direction].elevation
        );
    }

    public HexEdgeType GetEdgeType(HexCell otherCell)
    {
        return ECModel.instance.hexMetrics.GetEdgeType(
            elevation, otherCell.elevation
        );
    }

    public bool HasRiverThroughEdge(HexDirection direction)
    {
        return
            hasIncomingRiver && incomingRiver == direction ||
            hasOutgoingRiver && outgoingRiver == direction;
    }

    public void RemoveIncomingRiver()
    {
        if (!hasIncomingRiver)
        {
            return;
        }
        hasIncomingRiver = false;
        RefreshSelfOnly();

        HexCell neighbor = GetNeighbor(incomingRiver);
        neighbor.hasOutgoingRiver = false;
        neighbor.RefreshSelfOnly();
    }

    public void RemoveOutgoingRiver()
    {
        if (!hasOutgoingRiver)
        {
            return;
        }
        hasOutgoingRiver = false;
        RefreshSelfOnly();

        HexCell neighbor = GetNeighbor(outgoingRiver);
        neighbor.hasIncomingRiver = false;
        neighbor.RefreshSelfOnly();
    }

    public void RemoveRiver()
    {
        RemoveOutgoingRiver();
        RemoveIncomingRiver();
    }

    public void SetOutgoingRiver(HexDirection direction)
    {
        if (hasOutgoingRiver && outgoingRiver == direction)
        {
            return;
        }

        HexCell neighbor = GetNeighbor(direction);
        if (!IsValidRiverDestination(neighbor))
        {
            return;
        }

        RemoveOutgoingRiver();
        if (hasIncomingRiver && incomingRiver == direction)
        {
            RemoveIncomingRiver();
        }
        hasOutgoingRiver = true;
        outgoingRiver = direction;
        specialIndex = 0;

        neighbor.RemoveIncomingRiver();
        neighbor.hasIncomingRiver = true;
        neighbor.incomingRiver = direction.Opposite();
        neighbor.specialIndex = 0;

        SetRoad((int)direction, false);
    }

    public bool HasRoadThroughEdge(HexDirection direction)
    {
        return roads[(int)direction];
    }

    public void AddRoad(HexDirection direction)
    {
        if (
            !roads[(int)direction] && !HasRiverThroughEdge(direction) &&
            !IsSpecial && !GetNeighbor(direction).IsSpecial &&
            GetElevationDifference(direction) <= 1
        )
        {
            SetRoad((int)direction, true);
        }
    }

    public void RemoveRoads()
    {
        for (int i = 0; i < neighbors.Length; i++)
        {
            if (roads[i])
            {
                SetRoad(i, false);
            }
        }
    }

    public int GetElevationDifference(HexDirection direction)
    {
        int difference = elevation - GetNeighbor(direction).elevation;
        return difference >= 0 ? difference : -difference;
    }

    bool IsValidRiverDestination(HexCell neighbor)
    {
        return neighbor && (
            elevation >= neighbor.elevation || waterLevel == neighbor.elevation
        );
    }

    void ValidateRivers()
    {
        if (
            hasOutgoingRiver &&
            !IsValidRiverDestination(GetNeighbor(outgoingRiver))
        )
        {
            RemoveOutgoingRiver();
        }
        if (
            hasIncomingRiver &&
            !GetNeighbor(incomingRiver).IsValidRiverDestination(this)
        )
        {
            RemoveIncomingRiver();
        }
    }

    void SetRoad(int index, bool state)
    {
        roads[index] = state;
        neighbors[index].roads[(int)((HexDirection)index).Opposite()] = state;
        neighbors[index].RefreshSelfOnly();
        RefreshSelfOnly();
    }

    void RefreshPosition()
    {
        Vector3 position = transform.localPosition;
        position.y = elevation * ECModel.instance.hexMetrics.elevationStep;
        position.y +=
            (ECModel.instance.hexMetrics.SampleNoise(position).y * 2f - 1f) * 0.5f *
            ECModel.instance.hexMetrics.elevationPerturbStrength;
        transform.localPosition = position;

        Vector3 uiPosition = uiRect.localPosition;
        uiPosition.z = -position.y;
        uiRect.localPosition = uiPosition;
    }

    void Refresh()
    {
        if (chunk)
        {
            chunk.Refresh();
            for (int i = 0; i < neighbors.Length; i++)
            {
                HexCell neighbor = neighbors[i];
                if (neighbor != null && neighbor.chunk != chunk)
                {
                    neighbor.chunk.Refresh();
                }
            }
            //if (Unit) {
            //	Unit.ValidateLocation();
            //}
        }
    }

    void RefreshSelfOnly()
    {
        chunk.Refresh();
        //if (Unit) {
        //	Unit.ValidateLocation();
        //}
    }

    public void Save(BinaryWriter writer)
    {
        writer.Write((byte)terrainTypeIndex);
        writer.Write((byte)(elevation + 127));
        writer.Write((byte)waterLevel);
        writer.Write((byte)urbanLevel);
        writer.Write((byte)farmLevel);
        writer.Write((byte)plantLevel);
        writer.Write((byte)specialIndex);
        writer.Write(walled);

        if (hasIncomingRiver)
        {
            writer.Write((byte)(incomingRiver + 128));
        }
        else
        {
            writer.Write((byte)0);
        }

        if (hasOutgoingRiver)
        {
            writer.Write((byte)(outgoingRiver + 128));
        }
        else
        {
            writer.Write((byte)0);
        }

        int roadFlags = 0;
        for (int i = 0; i < roads.Length; i++)
        {
            if (roads[i])
            {
                roadFlags |= 1 << i;
            }
        }
        writer.Write((byte)roadFlags);
        writer.Write(IsExplored);
    }

    public void Load(BinaryReader reader, int header)
    {
        terrainTypeIndex = reader.ReadByte();
        ShaderData.RefreshTerrain(this);
        elevation = reader.ReadByte();
        if (header >= 4)
        {
            elevation -= 127;
        }
        RefreshPosition();
        waterLevel = reader.ReadByte();
        urbanLevel = reader.ReadByte();
        farmLevel = reader.ReadByte();
        plantLevel = reader.ReadByte();
        specialIndex = reader.ReadByte();
        walled = reader.ReadBoolean();

        byte riverData = reader.ReadByte();
        if (riverData >= 128)
        {
            hasIncomingRiver = true;
            incomingRiver = (HexDirection)(riverData - 128);
        }
        else
        {
            hasIncomingRiver = false;
        }

        riverData = reader.ReadByte();
        if (riverData >= 128)
        {
            hasOutgoingRiver = true;
            outgoingRiver = (HexDirection)(riverData - 128);
        }
        else
        {
            hasOutgoingRiver = false;
        }

        int roadFlags = reader.ReadByte();
        for (int i = 0; i < roads.Length; i++)
        {
            roads[i] = (roadFlags & (1 << i)) != 0;
        }

        IsExplored = header >= 3 ? reader.ReadBoolean() : false;
        //ShaderData.RefreshVisibility(this);
        ShaderData.RefreshAll(this);
    }

    public void SetLabel(string text)
    {
        UnityEngine.UI.Text label = uiRect.GetComponent<Text>();
        label.text = text;
    }

    public void DisableHighlight()
    {
        Image highlight = uiRect.GetChild(0).GetComponent<Image>();
        highlight.enabled = false;
    }

    public void EnableHighlight(Color color)
    {
        Image highlight = uiRect.GetChild(0).GetComponent<Image>();
        highlight.color = color;
        highlight.enabled = true;
    }

    //public void SetMapData(float data)
    //{
    //    ShaderData.SetMapData(this, data);
    //}


    //ECORRUPTION
    public ECCellM CreateAssociatedECHC(HexCell cell, int index)
    {
        ECCellV chosenPrefab;

        if (IsUnderwater)
        {
            chosenPrefab = ECModel.instance.referencesContainer.TryGetCellPrefab(ECCellM.HexCellType.Sea);
        }
        else if (HasRiver)
        {
            chosenPrefab = ECModel.instance.referencesContainer.TryGetCellPrefab(ECCellM.HexCellType.None);
        }
        else
        {
            chosenPrefab = ECModel.instance.referencesContainer
                .GetRandomPonderedCell();
        }

        ecCellV = Instantiate(chosenPrefab, transform.position, Quaternion.identity, transform);
        ecCellV.transform.localScale = 13 * Vector3.one;//JMPIZANA: only for demonstration purposes, delete in the future
        ecCellV.transform.localPosition = new Vector3(0f, -2.6f, 0f);//JMPIZANA: only for demonstration purposes, delete in the future
        ecCellV.Initialize(chosenPrefab.modelCell, cell, index);
        ecCellV.gameObject.name = "HCHexcell[" + coordinates.X + ", " + coordinates.Z + "]";

        //ShaderData.RefreshAll(this);

        return ecCellV.modelCell;
    }

    public ECCellM CreateAssociatedECHC(HexCell cell, ECCellSaveDataM cellSaveData)
    {
        ECCellM.HexCellType chosenType = (ECCellM.HexCellType)cellSaveData.chosenType;
        ECCellV chosenPrefab = ECModel.instance.referencesContainer.TryGetCellPrefab(chosenType);

        ecCellV = Instantiate(chosenPrefab, transform.position, Quaternion.identity, transform);
        ecCellV.transform.localScale = 13 * Vector3.one;//JMPIZANA: only for demonstration purposes, delete in the future
        ecCellV.transform.localPosition = new Vector3(0f, -2.6f, 0f);//JMPIZANA: only for demonstration purposes, delete in the future
        ecCellV.Initialize(chosenPrefab.modelCell, cell, cellSaveData.index);
        ecCellV.gameObject.name = "HCHexcell[" + coordinates.X + ", " + coordinates.Z + "]";

        //TODO: REHACER LA CARGA DEL DICCIONARIO A PARTIR DE UNA ARRAY DE STATE-VALUE-PAIR
        ecCellV.modelCell.cellState = new TotalStateDict(cellSaveData.cellState);

        if (cellSaveData.ongoingAction.actionData != null)
        {
            ECActionM ongoingAction = new ECActionM(cellSaveData.ongoingAction.actionData)
            {
                TurnCounter = cellSaveData.ongoingAction.turnCounter
            };

            ecCellV.modelCell.OngoingAction = ongoingAction;
        }

        //PFM: Only one effect per action
        if (cellSaveData.ongoingEffect.effectData != null)
        {
            ECEffectM ongoingEffect = new ECEffectM(cellSaveData.ongoingEffect.effectData);

            ecCellV.modelCell.OngoingEffect = ongoingEffect;
        }

        ecCellV.modelCell.blocked = cellSaveData.blocked;
        if (cellSaveData.blocked)
        {
            ecCellV.modelCell.mapCell.BlockedFactor = 1;
            //ecCellV.BlockState(1.0f);
        }

        return ecCellV.modelCell;
    }

}
