﻿using DG.Tweening;
using UnityEngine;

public class HexCellShaderData : MonoBehaviour
{
    const float transitionSpeed = 255f;

    Texture2D cellTexture;
    Color32[] cellTextureData;

    //List<HexCell> transitioningCells = new List<HexCell>();

    //bool needsVisibilityReset;

    public HexGrid Grid { get; set; }

    public bool ImmediateMode { get; set; }

    /// <summary>
    /// JMP: Moved information for cells that are updating to an external values
    /// array as it is not useful information for the shader.
    /// </summary>
    //bool[] updatingCells;

    /// <summary>
    /// JMP: 4 Tweeners per cell (4 channels of texture data)
    /// </summary>    
    Tweener[][] tweeners;
    int activeTweeners = 0;

    public void Initialize(int x, int z)
    {
        if (cellTexture)
        {
            cellTexture.Resize(x, z);
        }
        else
        {
            cellTexture = new Texture2D(
                x, z, TextureFormat.RGBA32, false, true
            );
            cellTexture.filterMode = FilterMode.Point;
            cellTexture.wrapModeU = TextureWrapMode.Repeat;
            cellTexture.wrapModeV = TextureWrapMode.Clamp;
            Shader.SetGlobalTexture("_HexCellData", cellTexture);
        }
        Shader.SetGlobalVector(
            "_HexCellData_TexelSize",
            new Vector4(1f / x, 1f / z, x, z)
        );

        if (cellTextureData == null || cellTextureData.Length != x * z)
        {
            cellTextureData = new Color32[x * z];
        }
        else
        {
            for (int i = 0; i < cellTextureData.Length; i++)
            {
                cellTextureData[i] = new Color32(0, 0, 0, 0);
            }
        }

        // updatingCells = new bool[x * z];
        tweeners = new Tweener[x * z][];
        for (int i = 0; i < tweeners.Length; i++)
        {
            tweeners[i] = new Tweener[4];
        }
        activeTweeners = 0;
        //transitioningCells.Clear();
        enabled = true;
    }

    public void RefreshAll(HexCell cell)
    {
        RefreshTerrain(cell);
        RefreshBlocked(cell);
        RefreshHighlight(cell);
    }

    public void RefreshTerrain(HexCell cell)
    {
        cellTextureData[cell.Index].a = (byte)cell.TerrainTypeIndex;
        enabled = true;
    }

    public void RefreshBlocked(HexCell cell) //JMP
    {
        int index = cell.Index;
        int targetNum = (int)(cell.BlockedFactor * 255);
        if (ImmediateMode)
        {
            cellTextureData[index].g = (byte)targetNum;
        }
        else
        {
            //GetterI getter = () => cellTextureData[index].g;
            //SetterI setter = (int v) => cellTextureData[index].g = (byte)v;
            if (tweeners[index][1] != null)
                tweeners[index][1].Kill(true);

            activeTweeners++;

            tweeners[index][1] = DOTween.To(
                 () => (int)cellTextureData[index].g,//Getter
                 (int b) => cellTextureData[index].g = (byte)b, //Setter
                targetNum, //FinalValue
                ECModel.instance.hexMetrics.visualTransitionSeconds //Duration
                )
                .OnComplete(() => activeTweeners--)
                ;

            //updatingCells[index] = true;
            //transitioningCells.Add(cell);
        }
        enabled = true;
    }

    public void RefreshHighlight(HexCell cell) //JMP
    {
        int index = cell.Index;
        int targetNum = (int)(cell.HighlightedFactor * 255);

        if (ImmediateMode)
        {
            cellTextureData[index].r = (byte)targetNum;
        }
        else //if (updatingCells[index] != true)
        {
            if (tweeners[index][0] != null)
                tweeners[index][0].Kill(true);

            activeTweeners++;


            tweeners[index][0] = DOTween.To(
                 () => (int)cellTextureData[index].r,//Getter
                 (int b) => cellTextureData[index].r = (byte)b, //Setter
                targetNum, //FinalValue
                ECModel.instance.hexMetrics.visualTransitionSeconds //Duration
                )
                .OnComplete(() => activeTweeners--)
                ;

            //updatingCells[index] = true;
            //transitioningCells.Add(cell);
        }
        enabled = true;
    }

    //public void RefreshVisibility(HexCell cell)
    //{
    //    int index = cell.Index;
    //    if (ImmediateMode)
    //    {
    //        //cellTextureData[index].r = cell.IsVisible ? (byte)255 : (byte)0;
    //        cellTextureData[index].g = cell.IsExplored ? (byte)255 : (byte)0;
    //    }
    //    else if (updatingCells[index] != true)
    //    {
    //        updatingCells[index] = true;
    //        transitioningCells.Add(cell);
    //    }
    //    //else if (cellTextureData[index].b != 255)
    //    //{
    //    //    cellTextureData[index].b = 255;
    //    //    transitioningCells.Add(cell);
    //    //}
    //    enabled = true;
    //}

    //public void SetMapData(HexCell cell, float data)
    //{
    //    cellTextureData[cell.Index].b =
    //        data < 0f ? (byte)0 : (data < 1f ? (byte)(data * 254f) : (byte)254);
    //    enabled = true;
    //}

    public void ViewElevationChanged()
    {
        //needsVisibilityReset = true;
        enabled = true;
    }

    void LateUpdate()
    {
        //if (needsVisibilityReset) {
        //	needsVisibilityReset = false;
        //	Grid.ResetVisibility();
        //}

        //int delta = (int)(Time.deltaTime * transitionSpeed);
        //if (delta == 0)
        //{
        //    delta = 1;
        //}
        //for (int i = 0; i < transitioningCells.Count; i++)
        //{
        //    if (!UpdateCellData(transitioningCells[i], delta))
        //    {
        //        transitioningCells[i--] =
        //            transitioningCells[transitioningCells.Count - 1];
        //        transitioningCells.RemoveAt(transitioningCells.Count - 1);
        //    }
        //}

        cellTexture.SetPixels32(cellTextureData);
        cellTexture.Apply();
        //Debug.Log("Active tweeners: " + activeTweeners, gameObject);
        enabled = activeTweeners > 0;
        //enabled = transitioningCells.Count > 0;
    }

    //bool UpdateCellData(HexCell cell, int delta)
    //{
    //    int index = cell.Index;
    //    Color32 data = cellTextureData[index];
    //    bool stillUpdating = false;

    //    /*JMP: Things to update: 
    //     * 1) Highlight (r)
    //     * 2) Blocked (b)
    //     * --------------
    //     * Terrain is handled instantly in RefreshTerrain();
    //     */

    //    if (cell.IsExplored && data.g < 255)
    //    {
    //        stillUpdating = true;
    //        int t = data.g + delta;
    //        data.g = t >= 255 ? (byte)255 : (byte)t;
    //    }

    //    //if (cell.IsVisible) {
    //    if (cell.IsHighlighted && data.r < 255)
    //    {
    //        stillUpdating = true;
    //        int t = data.r + delta;
    //        data.r = t >= 255 ? (byte)255 : (byte)t;
    //    }
    //    //}
    //    //else if (data.r > 0) {
    //    //	stillUpdating = true;
    //    //	int t = data.r - delta;
    //    //	data.r = t < 0 ? (byte)0 : (byte)t;
    //    //}

    //    if (!stillUpdating)
    //    {
    //        updatingCells[index] = false;
    //    }
    //    cellTextureData[index] = data;
    //    return stillUpdating;
    //}
}