﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class HexMapCamera : MonoBehaviour
{
    [Header("Controlled Movement")]
    public float stickMinZoom;
    public float stickMaxZoom;

    public float swivelMinZoom, swivelMaxZoom;

    public float moveSpeedMinZoom, moveSpeedMaxZoom;

    public float rotationSpeed;

    public float zoom;

    public float paddingDistanceMaxZoom, paddingDistanceMinZoom;

    [Header("Cell Approaching")]
    public float cellApproachSeconds = 1f;
    public AnimationCurve cellApproachCurve;
    public float cameraCenterVerticalOffset = -50f;


    DepthOfField dof = null;
    [Header("Depth of field")]
    public float minFocDist = 45.6f;
    public float maxFocDist = 70.0f;

    [Header("Fog Values")]
    public float minFog;
    public float maxFog;

    [Header("Radial menu scales")]
    public float minRadialMenuScale;
    public float maxRadialMenuScale;

    [Header("References")]
    public HexGrid grid;

    public Camera cam;

    public RectTransform radialMenu;

    Transform swivel, stick;

    float rotationAngle;

    static HexMapCamera instance;

    public static bool Locked
    {
        set
        {
            instance.enabled = !value;
        }
    }

    public static void ValidatePosition()
    {
        instance.AdjustPosition(0f, 0f);
    }

    void Awake()
    {
        instance = this;
        swivel = transform.GetChild(0);
        stick = swivel.GetChild(0);
        //MMP: Se saca el componente de profundidad de campo de la camara
        PostProcessVolume volume = cam.GetComponent<PostProcessVolume>();
        volume.profile.TryGetSettings<DepthOfField>(out dof);
    }


    void OnEnable()
    {
        //ValidatePosition();
    }

    public void UpdateDof()
    {
        //MMP: uso el valor de zoom para interpolar entre minFocDist y maxFocDist
        //esto aprovecha que zoom esta entre 0 y 1, si eso cambiara esto no funcionaria
        float focDist = minFocDist * zoom + maxFocDist * (1 - zoom); //podria aprovechar que 1-invZoom es zoom pero eso haria el codigo mas confuso

        dof.focusDistance.value = focDist;
    }

    public void UpdateFog()
    {
        float fog = maxFog * zoom + minFog * (1 - zoom);
        RenderSettings.fogDensity = fog;
    }

    public void UpdateRadialMenuScale()
    {
        float scale = minRadialMenuScale * zoom + maxRadialMenuScale * (1f - zoom);
        radialMenu.DOScale(new Vector3(scale, scale, scale), 0);
    }

    private void Update()
    {
        UpdateDof();
        UpdateFog();
        UpdateRadialMenuScale();
    }

    public void setFogValues(float min, float max)
    {
        minFog = min;
        maxFog = max;
    }


    public void ControlledMovement(float horizontal, float vertical)
    {
        float xDelta = horizontal * Mathf.Lerp(moveSpeedMinZoom, moveSpeedMaxZoom, zoom) * Time.deltaTime,
            zDelta = vertical * Mathf.Lerp(moveSpeedMinZoom, moveSpeedMaxZoom, zoom) * Time.deltaTime;

        AdjustPosition(xDelta, zDelta);
    }

    public void CenterOnCell(ECCellV viewCell)
    {
        //JMP: recuerda que mover a la posición de ViewCell funciona porque estamos moviendo el rig
        //y no la cámara en sí

        UpdateRadialMenuScale();

        Vector3 targetPosition = viewCell.transform.position 
            + transform.forward * cameraCenterVerticalOffset;

        targetPosition.y = 0;
        targetPosition = ClampPosition(targetPosition);

        transform.DOMove(
            targetPosition, cellApproachSeconds)
            .SetEase(cellApproachCurve);
    }

    public void AdjustZoom(float delta)
    {
        zoom = Mathf.Clamp01(zoom + delta);

        float distance = Mathf.Lerp(stickMinZoom, stickMaxZoom, zoom);
        stick.localPosition = new Vector3(0f, 0f, distance);

        float angle = Mathf.Lerp(swivelMinZoom, swivelMaxZoom, zoom);
        swivel.localRotation = Quaternion.Euler(angle, 0f, 0f);
    }

    public void AdjustRotation(float delta)
    {
        rotationAngle += delta * rotationSpeed * Time.deltaTime;
        if (rotationAngle < 0f)
        {
            rotationAngle += 360f;
        }
        else if (rotationAngle >= 360f)
        {
            rotationAngle -= 360f;
        }
        transform.localRotation = Quaternion.Euler(0f, rotationAngle, 0f);
    }

    void AdjustPosition(float xDelta, float zDelta)
    {
        Vector3 direction =
            transform.localRotation *
            new Vector3(xDelta, 0f, zDelta).normalized;
        float damping = Mathf.Max(Mathf.Abs(xDelta), Mathf.Abs(zDelta));
        float distance =
            Mathf.Lerp(moveSpeedMinZoom, moveSpeedMaxZoom, zoom) *
            damping * Time.deltaTime;

        Vector3 position = transform.localPosition;
        position += direction * distance;
        transform.localPosition =
            grid.wrapping ? WrapPosition(position) : ClampPosition(position);
    }

    Vector3 ClampPosition(Vector3 position)
    {
        float paddingDistance = Mathf.Lerp(paddingDistanceMinZoom, paddingDistanceMaxZoom, zoom);

        float xMax = (grid.cellCountX - 0.5f) * ECModel.instance.hexMetrics.InnerDiameter;
        position.x = Mathf.Clamp(position.x, 0f + paddingDistance, xMax - paddingDistance);

        float zMax = (grid.cellCountZ - 1) * (1.5f * ECModel.instance.hexMetrics.outerRadius);
        position.z = Mathf.Clamp(position.z, 0f + paddingDistance, zMax - paddingDistance);

        return position;
    }

    Vector3 WrapPosition(Vector3 position)
    {
        float width = grid.cellCountX * ECModel.instance.hexMetrics.InnerDiameter;
        while (position.x < 0f)
        {
            position.x += width;
        }
        while (position.x > width)
        {
            position.x -= width;
        }

        float zMax = (grid.cellCountZ - 1) * (1.5f * ECModel.instance.hexMetrics.outerRadius);
        position.z = Mathf.Clamp(position.z, 0f, zMax);

        grid.CenterMap(position.x);
        return position;
    }
}