﻿using UnityEngine;
using ECUtils;

public class ECCellV : MonoBehaviour
{
    public enum HighlightType { NONE, HALF, FULL }

    //referncia a su casilla lógica
    public ECCellM modelCell;

    public MusicSOController.AmbientClip ambientClip;

    MeshRenderer[] subMeshes;

    [HideInInspector]public ECExclamation exclamation;

    private void Awake()
    {
        subMeshes = GetComponentsInChildren<MeshRenderer>();
    }

    public void Initialize(ECCellM prefabCellModel, HexCell mapCell, int index)
    {
        //modelCell = new ECCellM(prefabCellModel, this, mapCell);

        modelCell.Initialize(prefabCellModel, this, mapCell, index);
    }

    public void Select()
    {
        //JMP: aquí en el futuro puede haber activación de animaciones, brillis y otros
    }

    public void Highlight(HighlightType type)
    {
        switch (type)
        {
            case HighlightType.NONE:
                modelCell.mapCell.DisableHighlight();
                modelCell.mapCell.HighlightedFactor = 0;
                break;

            case HighlightType.HALF:
                modelCell.mapCell.EnableHighlight(ECController.instance.hexUI.hexHighlightColor);
                modelCell.mapCell.HighlightedFactor = 0.3f;

                //JMP: cuando se pasa el raton por encima de la casilla de uan exclamación, desaparece.
                if (exclamation != null)
                {
                    Debug.Log("Deleting exclamation");
                    Destroy(exclamation.gameObject);
                }

                break;

            case HighlightType.FULL:
                modelCell.mapCell.EnableHighlight(ECController.instance.hexUI.hexHighlightColor);
                modelCell.mapCell.HighlightedFactor = 1f;
                break;
        }
    }

    //CLARA: Comentado porque ahora no se ponen en gris los modelos de casillas bloqueadas
    public void BlockState(float state)
    {

        //for (int c = 0; c < transform.childCount; c++)
        //{
        //    Transform child = transform.GetChild(c);

        //    for (int i = 0; i < child.childCount; i++)
        //        if (child.GetChild(i).GetComponent<MeshRenderer>() != null)
        //            if (child.GetChild(i).GetComponent<MeshRenderer>().material != null)
        //                child.GetChild(i).GetComponent<MeshRenderer>().material.SetFloat("_Blocked", state);
        //}
        
    }

    //CLARA:instanciación de la nueva casilla CAMBIAR NOMBRE
    public ECCellV SwitchCell(ECCellV newModelCell)
    {
        ECCellV newCellV = Instantiate(newModelCell,
            transform.parent.position,Quaternion.identity, transform.parent);

       // ECCellV newCellV = newCell.GetComponent<ECCellV>();
        //newCellV.subMeshes[0].transform.localPosition = new Vector3(0f, 0f, 0f);


        newCellV.transform.localScale = 13 * Vector3.one;//JMPIZANA: only for demonstration purposes, delete in the future
        newCellV.transform.localPosition = new Vector3(0f, -2.6f, 0f);//JMPIZANA: only for demonstration purposes, delete in the future
        newCellV.Initialize(newModelCell.modelCell, newCellV.modelCell.mapCell, modelCell.idListCellsM);
        newCellV.gameObject.name = this.gameObject.name;

        //newCellV.modelCell = ;
        newCellV.modelCell.mapCell = this.modelCell.mapCell;
        this.modelCell.mapCell.ecCellV = newCellV;
        modelCell.viewCell = newCellV;
        newCellV.modelCell.UpdateAvailableActions();
        newCellV.modelCell.UpdateCellVariations();
        ECModel.instance.UpdateRefCell(modelCell.idListCellsM, newCellV.modelCell);

        newCellV.exclamation = exclamation;

        Destroy(this.gameObject);
        //Debug.Log("Esto va bien");
        //TODO: hacer cositas bonitas de efectos

        return newCellV;
    }

}

