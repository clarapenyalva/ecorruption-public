﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ECDecisionV : MonoBehaviour
{
    public RectTransform decisionPanel;

    public RectTransform decisionButtonsPanel;
    [HideInInspector]public DecisionOptionButton[] decisionButtons;

    public Image decisionIcon;

    public TextMeshProUGUI decisionTitle;
    public TextMeshProUGUI decisionDescription;

    public Animator decisionPanelAnimator;

    public ECDecisionController decisionController;

    [Header("Prefabs")]
    public DecisionOptionButton optionButtonPrefab;

    //OPEN PHASE---------------------------------------------------------------------------
    public void StartDecision(ECDecisionM selectedDec)
    {
        LoadDecisionPanel(selectedDec);
        OpenDecisionpanel();

        //JMP: TODO: possible sound effect?
    }

    private void LoadDecisionPanel(ECDecisionM selectedDec)
    {
        //General description


        decisionDescription.text = selectedDec.description;
        decisionTitle.text = selectedDec.title;
        //Options buttons
        decisionButtons = new DecisionOptionButton[selectedDec.options.Length];
        for (int i = 0; i < selectedDec.options.Length; i++)
            decisionButtons[i] = AddOptionButton(selectedDec.options[i]);

        //Decision icon
        decisionIcon.sprite = selectedDec.icon;
    }
    
    private DecisionOptionButton AddOptionButton(ECDecisionOption option)
    {
        DecisionOptionButton butt = Instantiate(optionButtonPrefab, decisionButtonsPanel);

        butt.Initialize(option, this);

        //butt.buttonText.text = option.text;
        //butt.option = option;
        //butt.decision = this;

        return butt;
    }

    private void OpenDecisionpanel()
    {
        decisionPanelAnimator.SetBool("Open", true);
    }

    //CLOSE PHASE---------------------------------------------------------------------------
    public void SelectOption(ECDecisionOption selectedOption)
    {
        decisionController.ChoseDecision(selectedOption);
        CloseDecisionPanel();
    }

    private void CloseDecisionPanel()
    {
        decisionPanelAnimator.SetBool("Open", false);
    }

    public void OnDecisionPanelClosed()
    {
        UnloadDecisionPanel();
    }

    private void UnloadDecisionPanel()
    {
        for (int i = 0; i < decisionButtons.Length; i++)
        {
            Destroy(decisionButtons[i].gameObject);
        }
        decisionButtons = null;
    }
}
