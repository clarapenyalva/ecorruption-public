﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ECRadialMenuButtonV : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Image icon;
    public Image skinIcon;
    public ECRadialMenuV parentMenu;
    public ECActionM action;

    [HideInInspector] public Button thisButton;

    private void Awake()
    {
        thisButton = GetComponent<Button>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        transform.localScale = new Vector3(1.5f, 1.5f, 0.0f);
        parentMenu.FillInfoBox(action);

        //highlight action effects on resource panel
        //crisosie
        ResourcesData acEffects;
        acEffects = action.ReturnActionEffect();
        ECView.instance.HighlightResources(acEffects, action.enabledAction);

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        transform.localScale = new Vector3(1.0f, 1.0f, 0.0f);
        parentMenu.EmptyInfoBox();

        //hide action effects highlight on resource panel 
        //crisosie
        ECView.instance.HighlightResources(null, false);

    }

    public void OnClick()
    {
        //Debug.Log("Clik Event! " + action.actionD.name + ", " + action.actionD.description);
        //parentMenu.selectedAction = action;
        //ECView.instance.NextTurn();
        if (action.enabledAction)
        {
            parentMenu.ActionSelected(action);
        }
    }
}
