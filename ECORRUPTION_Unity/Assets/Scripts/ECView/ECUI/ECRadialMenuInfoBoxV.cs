﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(Animator))]
public class ECRadialMenuInfoBoxV : MonoBehaviour
{
    private const string animatorOpenPropertyName = "Open";

    [Header("References")]
    public TextMeshProUGUI textTitle;
    public TextMeshProUGUI textBody;

    public RectTransform textTimerContainer;
    public RectTransform imgTimerContainer;

    public TextMeshProUGUI textTimer;

    //public Image infoImage;
    private Animator animator;

    [HideInInspector] public bool isOpen = false;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        //infoImage = GetComponent<Image>();
    }

    public void SwapinfoMenu()
    {
        isOpen = !isOpen;
        animator.SetBool(animatorOpenPropertyName, isOpen);
    }

    public void OpenInfoMenu()
    {
        if (isOpen) return;

        isOpen = true;
        animator.SetBool(animatorOpenPropertyName, isOpen);
    }

    public void CloseInfoMenu()
    {
        if (!isOpen) return;

        isOpen = false;
        animator.SetBool(animatorOpenPropertyName, isOpen);
    }
}
