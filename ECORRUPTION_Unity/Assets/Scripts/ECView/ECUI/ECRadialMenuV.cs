﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* JMP:
 * RadialMenuV should start active because of limitations of the system:
 * Awake is not executed unless the object is enabled.
 */

[RequireComponent(typeof(Animator))]
public class ECRadialMenuV : MonoBehaviour
{
    public static ECRadialMenuV instance;
    private const string animatorOpenPropertyName = "Open";
    private const string animatorClosePropertyName = "Close";

    public enum RadialMenuOrientationMode { VERTICAL, LOOKING_CAMERA };

    [Header("Parameters")]
    public float menuRadius = 1f;
    [Range(0f, 10f)] public float heightOffset = 1f;
    public RadialMenuOrientationMode orientationMode = RadialMenuOrientationMode.VERTICAL;

    [Header("References")]
    [SerializeField] private ECRadialMenuInfoBoxV infoBox;
    //[HideInInspector]public ECActionM selectedAction;
    [SerializeField] private Image radialMenuRibbon;
    [SerializeField] private Transform animationContainer;
    public Image skinIcon;

    [Header("Prefabs")]
    public ECRadialMenuButtonV actionButtonPrefab;

    //public ECRadialMenuInfoBoxV infoBoxPrefab;

    [HideInInspector] private bool isOpen = false;
    public bool IsOpen { get { return isOpen; } }

    private ECCellV selectedCell;
    private List<ECRadialMenuButtonV> actionButtons;
    private Animator animator;
    private bool hasNextMenu = false;

    private void Awake()
    {
        if (instance != null && instance != this)
            Destroy(gameObject);
        else instance = this;

        animator = GetComponent<Animator>();
        //gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        OrientMenu();
    }

    public void ActionSelected(ECActionM action)
    {

        //Debug.Log("Menu Radial: Se ha seleccionado " + action.actionD.name + ", " + action.actionD.description);
        selectedCell.modelCell.ApplyAction(action);
        //ECView.instance.CellBlocked(selectedCell); //TODO [CLARA]
        ECView.instance.ManageSelectedAction(action);//JMP: here it calls next turn and so on            
    }

    private void OrientMenu()
    {
        switch (orientationMode)
        {
            case RadialMenuOrientationMode.VERTICAL:
                transform.LookAt(transform.position - Vector3.up, ECView.instance.camRig.transform.forward);
                break;

            case RadialMenuOrientationMode.LOOKING_CAMERA:
                //JMP: look to a point in the direction of the cam mirrored through the canvas.
                Vector3 camToCanvasDist = transform.position - ECView.instance.camRig.cam.transform.position;
                transform.LookAt(transform.position + camToCanvasDist);
                break;
        }
    }

    public void FillInfoBox(ECActionM action)
    {
        infoBox.textTitle.text = action.actionD.nameAction;
        infoBox.textBody.text = action.actionD.description;
        infoBox.textTimer.text = action.actionD.turnDuration.ToString();
        infoBox.textTimerContainer.gameObject.SetActive(true);
        infoBox.imgTimerContainer.gameObject.SetActive(true);
    }

    public void EmptyInfoBox()
    {
        if (selectedCell == null) return;
        infoBox.textTitle.text = selectedCell.modelCell.tileName;
        infoBox.textBody.text = selectedCell.modelCell.description;
        infoBox.textTimerContainer.gameObject.SetActive(false);
        infoBox.imgTimerContainer.gameObject.SetActive(false);
    }

    public bool SwitchMenu(ECCellV viewCell, List<ECActionM> actions)
    {
        if (isOpen) CloseMenu();
        else OpenMenu(viewCell, actions);

        return isOpen;
    }

    public Vector3 CurrentMenuPos
    {
        get { return selectedCell.transform.position + Vector3.up * heightOffset; }
    }

    private Color ReturnCellUIColor(ECCellM.HexCellType cellType)
    {
        Color c;
        switch (cellType)
        {
            case ECCellM.HexCellType.Wasteland:
                //c = new Color(0.804f, 0.0f, 0.408f);
                c = new Color(0.827f, 0.671f, 0.561f);
                break;
            case ECCellM.HexCellType.Forest:
                //c = new Color(0.169f, 0.702f, 0.0f);
                c = new Color(0.376f, 0.725f, 0.553f);
                break;
            case ECCellM.HexCellType.PowerPlant:
                //c = new Color(0.486f, 1.0f, 0.0f);
                //c = new Color(0.639f, 0.467f, 0.392f);
                c = new Color(0.702f, 0.718f, 0.651f);
                break;
            case ECCellM.HexCellType.City:
                c = new Color(0.565f, 0.753f, 0.788f);
                break;
            case ECCellM.HexCellType.Farm:
                c = new Color(0.929f, 0.898f, 0.565f);
                break;
            case ECCellM.HexCellType.IndustrialPark:
                c = new Color(0.858f, 0.596f, 0.576f);
                break;
            case ECCellM.HexCellType.Council:
                c = new Color(0.639f, 0.576f, 0.788f);
                break;
            default:
                c = new Color(0.992f, 0.659f, 0.663f);
                break;
        }
        return c;
    }

    List<ECActionM> delayedAcButtCreationList;
    public void OpenMenu(ECCellV viewCell, List<ECActionM> actions, bool fromPrevMenu = false)
    {
        if (isOpen) return;

        selectedCell = viewCell;

        ECCellM.HexCellType cellType = selectedCell.modelCell.cellType;
        Color UIColor = ReturnCellUIColor(cellType);
        radialMenuRibbon.color = UIColor;
        infoBox.GetComponent<Image>().color = UIColor;

        OrientMenu();

        if (!fromPrevMenu)
        {
            transform.position = CurrentMenuPos;
            CreateActionButtons(actions);
        }
        else
        {
            delayedAcButtCreationList = actions;
        }

        isOpen = true;
        //animator.SetBool(animatorOpenPropertyName, isOpen);
        animator.SetTrigger(animatorOpenPropertyName);
        EmptyInfoBox();
        infoBox.OpenInfoMenu();
    }

    public void CloseMenu(bool hasNextMenu = false)
    {
        if (!isOpen) return;

        this.hasNextMenu = hasNextMenu;

        selectedCell = null;
        isOpen = false;    
        //animator.SetBool(animatorOpenPropertyName, isOpen);
        animator.SetTrigger(animatorClosePropertyName);

        infoBox.CloseInfoMenu();        
    }

    public void OnMenuClosed()
    {
        //Destroy all actions (for now)
        //JMP: cambiado a for normal para comprobar bien nulos
        //actionButtons.ForEach((ECRadialMenuButtonV b) => Destroy(b.gameObject));

        for (int i = 0; i < actionButtons.Count; i++)
        {
            if (actionButtons[i] != null)
                Destroy(actionButtons[i].gameObject);
        }


        if (hasNextMenu)
        {
            transform.position = CurrentMenuPos;
            CreateActionButtons(delayedAcButtCreationList);
            hasNextMenu = false;
            delayedAcButtCreationList = null;
        }
    }

    private void CreateActionButtons(List<ECActionM> actions)
    {
        int numActions = actions.Count;
        actionButtons = new List<ECRadialMenuButtonV>(numActions);

        for (int i = 0; i < numActions; i++)
        {
            ECRadialMenuButtonV newAction = Instantiate(actionButtonPrefab) as ECRadialMenuButtonV;
            newAction.gameObject.SetActive(true);
            newAction.transform.SetParent(animationContainer, false);
            float theta = (2 * Mathf.PI / numActions) * i;
            float xPos = Mathf.Sin(theta);
            float yPos = Mathf.Cos(theta);
            newAction.transform.localPosition = new Vector3(xPos, yPos, 0f) * menuRadius;

            newAction.icon.sprite = actions[i].actionD.image;
            newAction.parentMenu = this;
            newAction.action = actions[i];

            //If action is not enabled due to money requirements
            if (!actions[i].enabledAction)
            {
                //newAction.enabled = false;
                newAction.icon.color = new Color(0.5f, 0.5f, 0.5f, 1f); // Set to opaque gray;
                //TODO: Poner background tambien en gris. No consigo acceder a el
            }

            actionButtons.Add(newAction);
        }

    }
}
