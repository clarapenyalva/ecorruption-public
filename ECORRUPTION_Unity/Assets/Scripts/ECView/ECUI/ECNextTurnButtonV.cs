﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Audio;

//https://docs.unity3d.com/ScriptReference/MonoBehaviour.OnMouseOver.html
[RequireComponent(typeof(Button))]
public class ECNextTurnButtonV : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    Button thisButton;

    public AudioClip hoverAudioClip;
    public AudioClip clickAudioClip;

    private void Start()
    {
        thisButton = GetComponent<Button>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        ECView.instance.soundManager.PlayFX(hoverAudioClip);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ECView.instance.soundManager.PlayFX(hoverAudioClip);
    }

    public void OnClick()
    {
        ECView.instance.soundManager.PlayFX(clickAudioClip);
        ECController.instance.NextTurn();
       
    }  
}
