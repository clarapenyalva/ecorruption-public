﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ECUtils;
using DG.Tweening;

/*Esto debe contener las barras de recurso y:
 * 1.- Representar los cambios en las barras (con animaciones, etc)
 *          Al inicio de cada turno, las barras cambian con la acción ejecutada
 *            y todos los efectos ogoing
 * 2.- Representar los potenciales cambios (en absoluto) que ejecutaría una acción en las barras.
 */

public class ECResourcesV : MonoBehaviour
{
    public static ECResourcesV instance;
    public Slider ecoSlider, bizSlider, favorSlider;
    public Slider moneySlider;

    public Image ecoImage, bizImage, favorImage, moneyImage;
    public Image ecoChange, bizChange, favorChange, moneyChangeUp, moneyChangeDown;

    public Image noMoneyCross;

    public float secondsToChangeBar = 1f;

    private int numArrow = 3;

    public void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            
        }
    }

    public void Start()
    {
        //CLARA: inicializar el intervalo de la interfaz
        float maxValue = ECView.instance.getMaxValueSlidesResources();
        ecoSlider.maxValue = maxValue;
        bizSlider.maxValue = maxValue;
        favorSlider.maxValue = maxValue;
        moneySlider.maxValue = maxValue;
        ecoSlider.minValue = 0.0f;
        bizSlider.minValue = 0.0f;
        favorSlider.minValue = 0.0f;
        moneySlider.minValue = 0.0f;
    }

    public void UpdateResources(ResourcesData resources)
    {
        DOTween.To(
            () => moneySlider.value, //Getter
            (float f) => moneySlider.value = f, //Setter
            resources.money, //FinalValue
            secondsToChangeBar //Time
            );

        DOTween.To(
            () => ecoSlider.value, //Getter
            (float f) => ecoSlider.value = f, //Setter
            resources.ecology, //FinalValue
            secondsToChangeBar //Time
            );

        DOTween.To(
            () => bizSlider.value, //Getter
            (float f) => bizSlider.value = f, //Setter
            resources.business, //FinalValue
            secondsToChangeBar //Time
            );

        DOTween.To(
            () => favorSlider.value, //Getter
            (float f) => favorSlider.value = f, //Setter
            resources.publicFavor, //FinalValue
            secondsToChangeBar //Time
            );

        //moneySlider.value = resources.money;
        //ecoSlider.value = resources.ecology;
        //bizSlider.value = resources.business;
        //favorSlider.value = resources.publicFavor;
    }

    public void UpdateGlobalResources(ResourcesData resourcesChange)
    {
        //Debug.Log("Money: " + resourcesChange.money + ", Eco: " + resourcesChange.ecology + ", Biz: " + resourcesChange.business + ", Favor: " + resourcesChange.publicFavor);

        Image[] images;
        images = moneyChangeUp.GetComponentsInChildren<Image>();
        foreach (Image im in images)
            im.enabled = false;

        images = moneyChangeDown.GetComponentsInChildren<Image>();
        foreach (Image im in images)
            im.enabled = false;

        float change = 0.0f;
        float negValueChangeRes = ECView.instance.getNegValueChangeResources();
        float posValueChangeRes = ECView.instance.getPosValueChangeResources();

        if (resourcesChange.money != 0)
        {
            bool negChange = (resourcesChange.money < 0.0) ? true : false;
            
            if (negChange)
            {
                change = getChangeInRange(-resourcesChange.money, ECView.instance.getNegValueChangeMoney(), numArrow);

                //enable down icons
                images = moneyChangeDown.GetComponentsInChildren<Image>();
                for (int i = 1; i < change + 1; i++)
                {
                    images[i].enabled = true;
                    images[i].color = new Color(1, 0, 0, 1);
                }
            }

            else
            {
                change = getChangeInRange(resourcesChange.money, ECView.instance.getPosValueChangeMoney(), numArrow);

                //enable up icons
                images = moneyChangeUp.GetComponentsInChildren<Image>();
                for (int i = 1; i < change + 1; i++)
                {
                    images[i].enabled = true;
                    images[i].color = new Color(0, 1, 0, 1);
                }
            }

            //Debug.Log("Money " + resourcesChange.money + " " + change);
        }

        images = bizChange.GetComponentsInChildren<Image>();
        foreach (Image im in images)
            im.enabled = false;
        if (resourcesChange.business != 0)
        {
            bool negChange = (resourcesChange.business < 0.0) ? true : false;
            //float change = Math.Abs(resourcesChange.business % 3);
            //if (resourcesChange.business >= 3) change = 3;
            
            if (negChange)
            {
                change = getChangeInRange(-resourcesChange.business, negValueChangeRes, numArrow);

                //enable down icons
                images = bizChange.GetComponentsInChildren<Image>();
                for (int i = 1; i < change + 1; i++)
                {
                    images[i].enabled = true;
                    images[i].color = new Color(1, 0, 0, 1);
                    images[i].transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 180.0f);
                }
            }
            else
            {
                change = getChangeInRange(resourcesChange.business, posValueChangeRes, numArrow);

                // enable up icons
                images = bizChange.GetComponentsInChildren<Image>();
                for (int i = 1; i < change + 1; i++)
                {
                    images[i].enabled = true;
                    images[i].color = new Color(0, 1, 0, 1);
                    images[i].transform.localRotation = Quaternion.identity; //((0.0f, 0.0f, 180.0f));
                }
            }

            //Debug.Log("Biz " + resourcesChange.business + " " + change);
        }

        images = ecoChange.GetComponentsInChildren<Image>();
        foreach (Image im in images)
            im.enabled = false;

        if (resourcesChange.ecology != 0)
        {
            bool negChange = (resourcesChange.ecology < 0.0) ? true : false;
                        
            if (negChange)
            {
                change = getChangeInRange(-resourcesChange.ecology, negValueChangeRes, numArrow);

                //enable down icons
                images = ecoChange.GetComponentsInChildren<Image>();
                for (int i = 1; i < change + 1; i++)
                {
                    images[i].enabled = true;
                    images[i].color = new Color(1, 0, 0, 1);

                    images[i].transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 180.0f);
                }
            }
            else
            {
                change = getChangeInRange(resourcesChange.ecology, posValueChangeRes, numArrow);

                // enable up icons
                images = ecoChange.GetComponentsInChildren<Image>();
                for (int i = 1; i < change + 1; i++)
                {
                    images[i].enabled = true;
                    images[i].color = new Color(0, 1, 0, 1);
                    images[i].transform.localRotation = Quaternion.identity; //((0.0f, 0.0f, 180.0f));

                }
            }

            //Debug.Log("Eco " + resourcesChange.ecology + " " + change);
        }

        images = favorChange.GetComponentsInChildren<Image>();
        foreach (Image im in images)
            im.enabled = false;

        if (resourcesChange.publicFavor != 0)
        {
            bool negChange = (resourcesChange.publicFavor < 0.0) ? true : false;

            if (negChange)
            {
                change = getChangeInRange(-resourcesChange.publicFavor, negValueChangeRes, numArrow);

                //enable down icons
                images = favorChange.GetComponentsInChildren<Image>();
                for (int i = 1; i < change + 1; i++)
                {
                    images[i].enabled = true;
                    images[i].color = new Color(1, 0, 0, 1);
                    images[i].transform.localRotation = Quaternion.Euler(0, 0, 180); //((0.0f, 0.0f, 180.0f));
                }
            }
            else
            {
                change = getChangeInRange(resourcesChange.publicFavor, posValueChangeRes, numArrow);

                // enable up icons
                images = favorChange.GetComponentsInChildren<Image>();
                for (int i = 1; i < change + 1; i++)
                {
                    images[i].enabled = true;
                    images[i].color = new Color(0, 1, 0, 1);
                    images[i].transform.localRotation = Quaternion.identity; //((0.0f, 0.0f, 180.0f));
                }
            }

            //Debug.Log("Favor " + resourcesChange.publicFavor + " " + change);
        }
    }

    public int getChangeInRange(float resourceChange, float posValueChangeRes, int numMaxInterv)
    {
        int value;
        float valInInterv = Mathf.Clamp((numMaxInterv * resourceChange / posValueChangeRes), 1, numMaxInterv);
        value = (int)Mathf.Round(valInInterv);

        return value;
    }

    //Highlights action effects on game.
    //crisosie
    public void HighlightResources(ResourcesData? resources, bool isAffordable)
    {
        if (resources!=null)
        {
            if (isAffordable)
            {
                float alpha;
                alpha = 0.2f * Math.Abs(resources.Value.ecology) % 3;
                //ecoImage.color = new Color(168f, 221f, 197f, 1.0f);
                //ecoImage.GetComponent<Image>().enabled = true;
                ecoImage.GetComponent<Image>().color = new Color(0.66f, 0.87f, 0.77f, alpha);

                alpha = 0.2f * Math.Abs(resources.Value.business) % 3;
                //bizImage.color = new Color(205f, 231f, 243f, 1.0f);
                //bizImage.GetComponent<Image>().enabled = true;
                bizImage.GetComponent<Image>().color = new Color(0.80f, 0.91f, 0.95f, alpha);

                alpha = 0.2f * Math.Abs(resources.Value.publicFavor) % 3;
                //favorImage.color = new Color(250f, 238f, 66f, 1.0f);
                //.GetComponent<Image>().enabled = true;
                favorImage.GetComponent<Image>().color = new Color(0.84f, 0.64f, 0.6f, alpha);

                alpha = 0.12f * Math.Abs(resources.Value.money) % 5;
                //moneyImage.color = new Color(213f, 162f, 153f, 1.0f);
                //moneyImage.GetComponent<Image>().enabled = true;
                moneyImage.GetComponent<Image>().color = new Color(0.98f, 0.93f, 0.26f, alpha);
            }
            else
            {
                noMoneyCross.gameObject.SetActive(true);
            }
        }
        else
        {
            ecoImage.GetComponent<Image>().color = new Color(0.66f, 0.87f, 0.77f, 0.0f);
            bizImage.GetComponent<Image>().color = new Color(0.80f, 0.91f, 0.95f, 0.0f);
            favorImage.GetComponent<Image>().color = new Color(0.98f, 0.93f, 0.26f, 0.0f);
            moneyImage.GetComponent<Image>().color = new Color(0.84f, 0.64f, 0.6f, 0.0f);
            noMoneyCross.gameObject.SetActive(false);
        }
    }
}
