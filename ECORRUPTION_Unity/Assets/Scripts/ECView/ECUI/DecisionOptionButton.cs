﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class DecisionOptionButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public Color availableColor = Color.white;
    public Color unavailableColor = new Color(0.722f, 0.722f, 0.722f);

    public TextMeshProUGUI buttonText;
    public Button button;
    public ECDecisionOption option;
    [HideInInspector] public ECDecisionV decision;

    bool IsAffordable
    {
        get
        {
            if (option.effectData.moneyChange < 0)
            {
                float monReq = Mathf.Abs(option.effectData.moneyChange * ECModel.instance.ResourcesM.Mult);
                if (monReq > ECModel.instance.GetResources().money)
                {
                    return false;
                }
            }
            return true;
        }
    }

    public void Initialize(ECDecisionOption option, ECDecisionV decision)
    {
        this.option = option;
        buttonText.text = option.text;
        this.decision = decision;

        if (IsAffordable)
            buttonText.color = availableColor;
        else
        {
            button.image.color = unavailableColor;
            buttonText.color = new Color(0.75f, 0.75f, 0.75f);
        }

    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        //JMP
        //TODO: visual effect
        //TODO: FX sound

        //highlight option effects on resource panel
        ResourcesData resChanges = new ResourcesData(0f, 0f, 0f, 0f);
        ECEffectM effect = new ECEffectM(this.option.effectData);
        effect.UpdateEffect(ref resChanges);

        ECView.instance.HighlightResources(resChanges, IsAffordable);

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //JMP
        //TODO: undo effect
        if (IsAffordable)
        {
            ECView.instance.HighlightResources(null, false);
        }
    }

    public void OnClick()
    {
        if (!IsAffordable) return;

        decision.SelectOption(option);
    }
}

