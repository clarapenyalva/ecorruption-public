﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ECCellTimerV : MonoBehaviour
{
    public static ECCellTimerV instance;

    private ECCellV selectedCell;
    private Vector3 originalPos;

    [Header("References")]
    public TextMeshProUGUI timerText;

    [Header("Parameters")]
    [Range(0f, 20f)] public float heightOffset = 1f;

    private void Awake()
    {
        if (instance != null && instance != this)
            Destroy(gameObject);
        else instance = this;

        this.gameObject.SetActive(false);
        originalPos = transform.position;
    }

    public void OpenCellTimer(ECCellV viewCell)
    {
        selectedCell = viewCell;

        ECCellM cellM = selectedCell.modelCell;

        if(cellM.OngoingAction != null)
        {
            timerText.text = cellM.OngoingAction.BlockedTurns().ToString();
            transform.position = CurrentCellTimerPos;
            Vector3 camToCanvasDist = transform.position - ECView.instance.camRig.cam.transform.position;
            transform.LookAt(transform.position + camToCanvasDist);

            this.gameObject.SetActive(true);
        }
        else
        {
            CloseCellTimer();
        }
    }

   Vector3 CurrentCellTimerPos
    {
        //get { return selectedCell.transform.position; }
        get { return selectedCell.transform.position + Vector3.up * heightOffset; }
    }

    void CloseCellTimer()
    {
        this.gameObject.SetActive(false);
        transform.position = originalPos;
    }
}
