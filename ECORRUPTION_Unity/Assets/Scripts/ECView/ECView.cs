﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*Esta clase se va a encargar de:
 * 1.- Almacenar los datos únicamaente relacionados con vista a interacción (prefabs de paneles, etc).  
 *          1.a.- Estos datos pueden estar encapsulados en clases más grandes, no hace falta cada referencia a cada botoncillo.
 * 2.- Comunicarse con ECController
 */

class ECView : MonoBehaviour
{
    public static ECView instance;

    [Header("References")]
    [HideInInspector] public ECRadialMenuV radialMenu;
    public HexMapCamera camRig;
    public MusicSOController soundManager;

    [HideInInspector] public ECController controller;
    [SerializeField] private ECResourcesV resourcesV;
    private ECRadialMenuButtonV rmButtonV;

    [HideInInspector] public ECCellTimerV cellTimer; //PFM: cell timer

    ECCellV hoveredCell;
    ECCellV prevHoveredCell;
    ECCellV selectedCell;

    [Header("Exclamation")]
    public Transform exclamationParent;
    public ECExclamation exclamationPrefab;

    private List<ECExclamation> exclamations;
    private List<ECCellV> cellsToExclamate;
    //referencia a radial menu, etc.

    public void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
        controller = GetComponent<ECController>();
        //resourcesV = new ECResourcesV();
        exclamations = new List<ECExclamation>();
        cellsToExclamate = new List<ECCellV>();
    }

    private void Start()
    {
        radialMenu = ECRadialMenuV.instance;
        //resourcesV = ECResourcesV.instance;
        cellTimer = ECCellTimerV.instance; //PFM: cell timer
    }

    public void UpdateResources(ResourcesData resources)
    {
        ECResourcesV.instance.UpdateResources(resources);
    }

    public float getMaxValueSlidesResources()
    {
        return ECModel.instance.getMaxValueSlidesResources();
    }

    public float getPosValueChangeResources()
    {
        return ECModel.instance.getPosValueChangeResources();
    }

    public float getNegValueChangeResources()
    {
        return ECModel.instance.getNegValueChangeResources();
    }

    public float getPosValueChangeMoney()
    {
        return ECModel.instance.getPosValueChangeMoney();
    }

    public float getNegValueChangeMoney()
    {
        return ECModel.instance.getNegValueChangeMoney();
    }

    //Cristian
    public void UpdateGlobalResources(ResourcesData resources)
    {
        ECResourcesV.instance.UpdateGlobalResources(resources);
    }

    public void UpdateView()
    {
        //Debug.Log("UpdateView in ECView not implemented yet!");
    }

    public HexCell GetCell(Ray ray)
    {
        return controller.grid.GetCell(ray);
    }

    public void OnHoverOnMap(ECCellV viewCell)
    {
        prevHoveredCell = hoveredCell;
        hoveredCell = viewCell;

        if (hoveredCell != null && prevHoveredCell != hoveredCell)
        {
            if (prevHoveredCell && prevHoveredCell != selectedCell)
                prevHoveredCell.Highlight(ECCellV.HighlightType.NONE);

            if (hoveredCell &&
                !hoveredCell.modelCell.blocked &&
                hoveredCell.modelCell.HasAvailableActions() &&
                hoveredCell != selectedCell)
                hoveredCell.Highlight(ECCellV.HighlightType.HALF);

            cellTimer.OpenCellTimer(viewCell); //PFM: cell timer
        }
        else if (prevHoveredCell != null && hoveredCell == null)
        {
            prevHoveredCell.Highlight(ECCellV.HighlightType.NONE);
        }
    }

    public void OnClickOnMap(ECCellV viewCell)
    {
        //JMP: If radial menu is closed
        //if (!radialMenu.IsOpen)
        //{
        //if (viewCell)
        //{
        if (viewCell != null && !viewCell.modelCell.blocked
            && hoveredCell.modelCell.HasAvailableActions()
            && viewCell != selectedCell
            )
        {
            bool fromPrevMenu = radialMenu.IsOpen;
            if (fromPrevMenu)
            {
                if (selectedCell)
                {
                    selectedCell.Highlight(ECCellV.HighlightType.NONE);
                    selectedCell = null;
                }

                //JMP: we close here just for safety
                InputManager.instance.lockedCamera = false;
                radialMenu.CloseMenu(true);
            }
            selectedCell = viewCell;
            selectedCell.Highlight(ECCellV.HighlightType.FULL);
            OnClickOnCell(viewCell, fromPrevMenu);
            soundManager.PlayAmbient(viewCell.ambientClip);
        }
        //}
        else
        {
            if (selectedCell)
            {
                selectedCell.Highlight(ECCellV.HighlightType.NONE);
                selectedCell = null;
            }

            //JMP: we close here just for safety
            InputManager.instance.lockedCamera = false;
            radialMenu.CloseMenu(false);
            soundManager.StopAmbient();
        }
        //}
        //else
        //{
        //    selectedCell.Highlight(ECCellV.HighlightType.NONE);
        //    selectedCell = null;
        //    InputManager.instance.lockedCamera = false;
        //    radialMenu.CloseMenu();
        //}
    }

    private void OnClickOnCell(ECCellV viewCell, bool fromPrevMenu)
    {
        viewCell.Select();
        controller.SelectCell(viewCell.modelCell); //JMP: Aquí actualiza las Acciones disponibles

        //centrar la cámara en la casilla (llamando a view manager)
        radialMenu.OpenMenu(viewCell, viewCell.modelCell.getAvailableActions(), fromPrevMenu);

        InputManager.instance.lockedCamera = true;
        camRig.CenterOnCell(viewCell);
    }

    public void ManageSelectedAction(ECActionM action)
    {
        //Efectos, etc
        selectedCell.Highlight(ECCellV.HighlightType.NONE);
        //selectedCell.BlockState(1.0f);
        selectedCell = null;
        InputManager.instance.lockedCamera = false;
        radialMenu.CloseMenu();
        ECView.instance.soundManager.StopAmbient();
        NextTurn();
    }

    public void NextTurn()
    {
        PrepareExclamation();
        controller.NextTurn(); //gestiona el cambio de datos internos

        Exclamate();
    }

    private void PrepareExclamation()
    {
        //Debug.Log("Pre-preparing exclamation lists");
        for (int i = 0; i < exclamations.Count; i++)
        {
            if(exclamations[i] != null)            
                Destroy(exclamations[i].gameObject);            
        }

        exclamations.Clear();
        cellsToExclamate.Clear();
    }

    private void Exclamate()
    {
        Debug.Log("EXCLAMATING: " + cellsToExclamate.Count);

        for (int i = 0; i < cellsToExclamate.Count; i++)
        {
            ECCellV cell = cellsToExclamate[i];
            ECExclamation exclamation = Instantiate(exclamationPrefab, 
                cell.transform.position, Quaternion.identity, exclamationParent);
            cell.exclamation = exclamation;
            exclamations.Add(exclamation);
            //Debug.Log("Instancing exclamation");
        }
    }

    public void CompletedAction(ECCellV cellV)
    {
        //Es llamado dentro de controller.NextTurn();
        //cellV.BlockState(0.0f);
        cellsToExclamate.Add(cellV);
        //Debug.Log("Added cell to Exclamate");
    }

    //JMP: Movido directamente a las celdas por sencillez
    //public void CellBlocked(ECCellV cellV)
    //{
    //    //TODO: cambiar visualización de celda para que se note que está bloqueada [CLARA]

    //}

    //Calls ECResourceV function that highlights action effects on game.
    //Crisosie
    public void HighlightResources(ResourcesData? resources, bool isAffordable)
    {
        ECResourcesV.instance.HighlightResources(resources, isAffordable);
    }

}
