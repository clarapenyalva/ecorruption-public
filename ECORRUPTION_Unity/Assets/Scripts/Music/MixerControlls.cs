﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class MixerControlls : MonoBehaviour
{
    public AudioMixer mixer;

    public void SetMasterVolume(Slider masterVolume)
    {
        mixer.SetFloat("MasterVolume", Mathf.Log10(masterVolume.value) * 20);
        PlayerPrefs.SetFloat("MasterVolume", masterVolume.value);
    }

    public void SetMasterVolume(float masterVolume)
    {
        mixer.SetFloat("MasterVolume", Mathf.Log10(masterVolume) * 20);
        PlayerPrefs.SetFloat("MasterVolume", masterVolume);
    }

    public void SetMusicVolume(Slider musicVolume)
    {
        mixer.SetFloat("MusicVolume", Mathf.Log10(musicVolume.value) * 20);
        PlayerPrefs.SetFloat("MusicVolume", musicVolume.value);

    }

    public void SetMusicVolume(float musicVolume)
    {
        mixer.SetFloat("MusicVolume", Mathf.Log10(musicVolume) * 20);
        PlayerPrefs.SetFloat("MusicVolume", musicVolume);

    }

    public void SetAmbientVolume(Slider ambientVolume)
    {
        mixer.SetFloat("AmbientVolume", Mathf.Log10(ambientVolume.value) * 20);
        PlayerPrefs.SetFloat("AmbientVolume", ambientVolume.value);

    }

    public void SetAmbientVolume(float ambientVolume)
    {
        mixer.SetFloat("AmbientVolume", Mathf.Log10(ambientVolume) * 20);
        PlayerPrefs.SetFloat("AmbientVolume", ambientVolume);

    }

    public void SetFXVolume(Slider fxVolume)
    {
        mixer.SetFloat("FXVolume", Mathf.Log10(fxVolume.value) * 20);
        PlayerPrefs.SetFloat("FXVolume", fxVolume.value);

    }

    public void SetFXVolume(float fxVolume)
    {
        mixer.SetFloat("FXVolume", Mathf.Log10(fxVolume) * 20);
        PlayerPrefs.SetFloat("FXVolume", fxVolume);

    }


}
