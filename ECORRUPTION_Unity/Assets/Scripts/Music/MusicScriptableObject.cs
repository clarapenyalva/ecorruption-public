﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "MusicClips", order = 1)]
public class MusicScriptableObject : ScriptableObject
{
    public AudioClip[] musicClip;
    public AudioClip[] ambientClips;
    public AudioClip[] fxClips;
}
