﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicSOController : MonoBehaviour
{
    public MusicScriptableObject mySO;

    public enum AmbientClip { Wasteland, City, Forest, Government, Farm, Industry, Powerplant};

    private AudioSource source;
    private float volLowRange = 0.5f;
    private float volHighRange = 1.0f;

    private AudioSourcePool pool;
    public AudioSource prefab;
    public int sourcePoolSize;


    void Awake()
    {
        source = GetComponent<AudioSource>();
        pool = new AudioSourcePool(prefab, sourcePoolSize);
    }

    public void PlayAmbient(AmbientClip am)
    {
        source.clip = mySO.ambientClips[(int)am];
        source.Play();
    }

    public void StopAmbient()
    {
        source.Stop();
        source.clip = null;
    }

    public void PlayFX(AudioClip clip)
    {
        pool.PlayClip(clip);
    }


    public class AudioSourcePool
    {
        public AudioSource prefab;

        private LinkedListNode<AudioSource> current;

        LinkedList<AudioSource> audioSources;

        Dictionary<AudioSource, bool> inUse;

        int index = 0;

        public AudioSourcePool(AudioSource prefab, int poolSize)
        {
            audioSources = new LinkedList<AudioSource>();
            inUse = new Dictionary<AudioSource, bool>();
            this.prefab = prefab;

            for (int i = 0; i < poolSize; i++)
            {
                AudioSource instance = Instantiate(prefab);
                instance.name = "Source" + i;
                audioSources.AddLast( instance);
                inUse.Add(instance, false);
            }
            current = audioSources.Last;
            index = 0;
        }

        public void PlayClip(AudioClip clip)
        {
            //actualiza la lista de fuentes en uso
            foreach (AudioSource key in new List<AudioSource>(inUse.Keys))
            {
                if (key != null)
                    inUse[key] = key.isPlaying;
                else
                    return;
            }

            //Reproduce el clip usando la fuente actual
            AudioSource sorse = current.Value;
            sorse.clip = clip;
            sorse.Play();

            //Se marca como fuente actual la siguiente que no está ocupada
            if (current.Next == null)
            {
                //si se ha alcanzado el final de la lista se salta al principio
                current = audioSources.First;
                index = 0;
            }
            else
            {
                current = current.Next;
                index++;
            }
            //Se comprueba que la fuente escogida no está ya ocupada
            while(inUse[current.Value])
            {
                if (current.Next == null)
                {
                    current = audioSources.First;
                    index = 0;
                }
                else
                {
                    current = current.Next;
                    index++;
                }
            }
        }
    }
}
