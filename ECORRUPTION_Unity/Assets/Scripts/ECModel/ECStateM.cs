﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new State", menuName = "EC/State"), System.Serializable]
public class ECStateM : ScriptableObject
    //, IEquatable<ECStateM>
{
    public string nameState;
    public int initialValue;

    public ECStateM(string nameState, int initialValue)
    {
        this.nameState = nameState;
        this.initialValue = initialValue;
    }

    //public override bool Equals(object obj)
    //{
    //    return Equals(obj as ECStateM);
    //}

    //public bool Equals(ECStateM other)
    //{
    //    return other != null &&
    //           base.Equals(other) &&
    //           nameState == other.nameState;
    //}

    //public override int GetHashCode()
    //{
    //    var hashCode = -646551375;
    //    hashCode = hashCode * -1521134295 + base.GetHashCode();
    //    hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(nameState);
    //    return hashCode;
    //    //return nameState.GetHashCode();
    //}

    //public override string ToString()
    //{
    //    return nameState;
    //}

    //public static bool operator ==(ECStateM data1, ECStateM data2)
    //{
    //    return EqualityComparer<ECStateM>.Default.Equals(data1, data2);
    //}

    //public static bool operator !=(ECStateM data1, ECStateM data2)
    //{
    //    return !(data1 == data2);
    //}
}

