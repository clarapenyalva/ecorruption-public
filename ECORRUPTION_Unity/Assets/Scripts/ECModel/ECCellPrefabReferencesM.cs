﻿using UnityEngine;

[CreateAssetMenu(fileName = "new Prefab References Container", menuName = "Prefab References Container")]
public class ECCellPrefabReferencesM : ScriptableObject
{
    // public ECHexCell ecHexCellPrefab;
    //public ECHexCell ecHexCellPrefabCube;
    [Range(0f, 1f)]
    public float cellsToTilesRatio = 0.6f;
    [SerializeField] private PonderedCellArray casillasPrefabPesos;
    //[SerializeField] public ECCellsPrefabsDict casillasPrefab;
    //public ECCellV fallbackPrefab;

    public void Initialize()
    {
        casillasPrefabPesos.Initialize();
    }

    [System.Serializable]
    public class PonderedCellArray
    {
        [SerializeField] private TypeCellWeightTriplet[] cellsWeights;
        [SerializeField] private ECCellV fallbackPrefab;

        private bool initialized = false;
        private int totalWeight = -1;

        public ECCellV TryGetCellPrefab(ECCellM.HexCellType type)
        {
            ECCellV value = null;

            for (int i = 0; i < cellsWeights.Length; i++)
            {
                if (cellsWeights[i].first == type)
                {
                    value = cellsWeights[i].second;
                }
            }

            if (value == null) value = fallbackPrefab;

            return value;
        }


        public ECCellV GetRandomPonderedCell(float chanceOfNoCell)
        {
            if (!initialized) Initialize();


            if (UnityEngine.Random.Range(0f, 1f) < chanceOfNoCell)
                return fallbackPrefab;

            float selected = UnityEngine.Random.Range(0f, (float)totalWeight);

            int currentW = 0;
            for (int i = 0; i < cellsWeights.Length; i++)
            {
                currentW += cellsWeights[i].third;
                if (currentW > selected)
                {
                    return cellsWeights[i].second;
                }
            }

            return fallbackPrefab;
        }

        public void Initialize()
        {
            totalWeight = 0;

            for (int i = 0; i < cellsWeights.Length; i++)
            {
                totalWeight += cellsWeights[i].third;
            }
            initialized = true;
        }
    }

    // public ECHexCell ecHexCellPrefab;
    //public ECHexCell ecHexCellPrefabCube;

    //[SerializeField] private PonderedCellArray casillasPrefabPesos;
    //[SerializeField] public ECCellsPrefabsDict casillasPrefab;
    //public ECCellV fallbackPrefab

    [System.Serializable]
    public class TypeCellWeightTriplet : Triplet<ECCellM.HexCellType, ECCellV, int>
    {
        public TypeCellWeightTriplet(ECCellM.HexCellType first, ECCellV second, int third) : base(first, second, third)
        {
        }
    }

    public class CellWeightPair : Pair<ECCellV, int>
    {
        public CellWeightPair(ECCellV first, int second) : base(first, second)
        {
        }
    }

    [System.Serializable]
    public class ECCellsPrefabsDict : SerializableDictionary<ECCellM.HexCellType, ECCellV>
    { }

    //public ECCellV TryGetCellPrefab(ECCellM.HexCellType type)
    //{
    //    ECCellV value;

    //    if(!casillasPrefab.TryGetValue(type, out value))
    //    {
    //        value = fallbackPrefab;
    //    }

    //    return value;
    //    //CAMBAIR ESTO A TRYCOSAS
    //    //try
    //    //{
    //    //    return casillasPrefab[type];
    //    //}
    //    //catch (KeyNotFoundException e)
    //    //{
    //    //    return fallbackPrefab;
    //    //}
    //}

    public ECCellV TryGetCellPrefab(ECCellM.HexCellType type)
    {
        return casillasPrefabPesos.TryGetCellPrefab(type);
    }

    public ECCellV GetRandomPonderedCell()
    {
        return casillasPrefabPesos.GetRandomPonderedCell(cellsToTilesRatio);
    }
}
