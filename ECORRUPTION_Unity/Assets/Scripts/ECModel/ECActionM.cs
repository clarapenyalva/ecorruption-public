﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECUtils;

public class ECActionM
{
    public ECActionD actionD;
    public ECEffectM effectM; //PFM: Only one effect per action
    public TotalStateDict stateRequirements;
    public TotalStateDict preStateChanges;
    public TotalStateDict postStateChanges;
    public int TurnCounter  {
        get { return turnCounter; }
        set { turnCounter = value; }
    } 

    [SerializeField]
    private int turnCounter = 0;
    public bool enabledAction = false;

    public ECActionM(ECActionD data) {

        actionD = data;
        effectM = new ECEffectM(actionD.effectD);
        stateRequirements = new TotalStateDict(data.stateRequirementsPrototype);
        try
        {
            preStateChanges = new TotalStateDict(data.preStateChangesPrototype); //PFM: Cambio de modelo al iniciar accion
            postStateChanges = new TotalStateDict(data.postStateChangesPrototype);
        }catch(System.NullReferenceException e)
        {
            Debug.Log(actionD.nameAction);
        }
    }

    public bool UpdateAction()
    {
        turnCounter++;
        return turnCounter >= actionD.turnDuration;
    }

    public bool CheckRequirements(ECCellM cell, ECResourcesM globalGameState)
    {
        //check cell state
        foreach (string reqStateName in stateRequirements.Keys)
        {
            int cellStateValue;

            if (!cell.cellState.TryGetValue(reqStateName, out cellStateValue))
            {
                return false; //Check if the required state exists in the cell
            }
            else
            {
                int stateCondition;
                //stateRequirements.TryGetValue(reqStateName, out stateCondition);
                stateCondition = stateRequirements[reqStateName];
                if (cellStateValue != stateCondition) //Check if the values for the requirement are the same 
                {
                    return false;
                }
            }
        }

        //check money requirement
        //PFM: El Dinero en base de datos esta en coste por turno base
        float moneyReq = actionD.moneyRequirement * actionD.turnDuration * globalGameState.Mult;
        if (moneyReq == 0 || (moneyReq <= globalGameState.resources.money))
        {
            enabledAction = true;
        }
        else
        {
            enabledAction = false;
        }

        //TODO: descomentar cuando tengamos toda la conexión del estado global del juego
        //check world state
        //foreach (ECStateM actionState in actionD.stateRequirements.Keys)
        //{
        //    int globalStateValue;

        //    if (!globalGameState.globalState.TryGetValue(actionState, out globalStateValue))
        //    {
        //        return false; //Check if the required state exists in the global state
        //    }
        //    else
        //    {
        //        int stateCondition;
        //        actionD.stateRequirements.TryGetValue(actionState, out stateCondition);
        //        if (globalStateValue != stateCondition) //Check if the values for the requirement are the same 
        //        {
        //            return false;
        //        }
        //    }
        //}


        //All state requirements are being fulfilled
        return true;
    }


    //Returns array with the total effect of a certain action on the resources state.
    //Crisosie
    public ResourcesData ReturnActionEffect()
    {
        ResourcesData acEffects = new ResourcesData(0,0,0,0);
        //PFM: Only one effect per action. It's not necessary to check whether there is an effect, because every action must have an effect
        acEffects.ecology += effectM.effectD.ecologyChange;
        acEffects.business += effectM.effectD.businessChange;
        acEffects.publicFavor += effectM.effectD.populationChange;
        acEffects.money += effectM.effectD.moneyChange;

        return acEffects;
    }

    public int BlockedTurns()
    {
        return actionD.turnDuration - TurnCounter;
    }
}
