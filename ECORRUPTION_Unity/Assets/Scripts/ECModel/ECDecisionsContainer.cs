﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable, CreateAssetMenu(fileName = "NewDecisionContainer", menuName = "EC/DecisionContainer")]
public class ECDecisionsContainer : ScriptableObject
{
    public List<ECDecisionM> decisions;
}
