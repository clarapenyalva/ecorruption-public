﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECUtils;

[CreateAssetMenu(fileName = "new Action", menuName = "EC/Action"), System.Serializable]
public class ECActionD : ScriptableObject
{
    public string nameAction = "";
    [TextArea(15, 20)]
    public string description = "";
    public float moneyRequirement = 0;
    public int turnDuration = 0;
    public Sprite image; 
    public StateValuePair[] stateRequirementsPrototype;
    public StateValuePair[] preStateChangesPrototype; //PFM: Cambio de modelo al iniciar la accion
    public StateValuePair[] postStateChangesPrototype;
    public ECEffectD effectD; //PFM: Only one effect per action
    public ECCellV changeCell;
}
