
using ECUtils;
using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


[Serializable]
public class ECCellM
{
    public enum HexCellType { None, Wasteland, Forest, PowerPlant, City, Sea, Mountain, SolarPlant, Farm, Council, IndustrialPark };

    //HexCellType
    [SearchableEnum]
    public HexCellType cellType;
    public string tileName;
    [TextArea(15, 20)]
    public string description = "";
    public List<ECActionD> allActionsData;

    public StateValuePair[] cellStatePrototype;

    //JMP: this should not be edited from the editor, but loaded from a 
    //StateValuePair list or array
    /*[HideInInspector]*/
    [SerializeField] public TotalStateDict cellState;

    [HideInInspector] public ECCellV viewCell;

    [HideInInspector]
    public HexCell mapCell;
    [HideInInspector] public bool blocked = false; //JMP: bloquear

    [HideInInspector]
    public List<ECActionM> allActions;

    public ECActionM OngoingAction { get { return ongoingAction; } set { ongoingAction = value; } }
    public ECEffectM OngoingEffect { get { return ongoingEffect; } set { ongoingEffect = value; } } //PFM: Only one effect per action

    protected List<ECActionM> availableActions;
    protected ECActionM ongoingAction;
    protected ECEffectM ongoingEffect; //PFM: Only one effect per action

    /*
     * This class contains the list of models in the cell that can
     * be made visible/invisible depending on the cell state
     */
    [System.Serializable]
    public class ModelStateContainer
    {
        public Transform model;
        public StateValuePair state;
    }

    public ModelStateContainer[] cellVariations;

    [HideInInspector] public int idListCellsM;

    public void Initialize(ECCellM prefab, ECCellV viewCell, HexCell mapCell, int index)
    {
        cellState = new TotalStateDict(prefab.cellStatePrototype);
        this.viewCell = viewCell;
        this.mapCell = mapCell;
        blocked = false;

        allActions = new List<ECActionM>();
        foreach (ECActionD actD in allActionsData)
        {
            allActions.Add(new ECActionM(actD));
        }
        availableActions = new List<ECActionM>();
        ongoingAction = null;
        ongoingEffect = null;
        idListCellsM = index;
    }

    //Change type cell in game mode (produced by action)
    public void ChangeCellInGrid(ECCellV newRefCellV)
    {
        viewCell =  viewCell.SwitchCell(newRefCellV);
    }

    //Change type cell in game mode (produced by action)
    //CLARA: versi�n de cambiado cuando no se activaban/desactivaban modelos de casillas
    public void ChangeCellInGrid_OLD(ECCellV newViewCell, GameObject newModelCell)
    {
        this.cellState = new TotalStateDict(newViewCell.modelCell.cellStatePrototype);
        this.cellStatePrototype = newViewCell.modelCell.cellStatePrototype;
        this.cellType = newViewCell.modelCell.cellType;
        this.tileName = newViewCell.modelCell.tileName;
        this.description = newViewCell.modelCell.description;
        this.blocked = false;
        this.allActionsData = newViewCell.modelCell.allActionsData;
        this.allActions = new List<ECActionM>();

        foreach (ECActionD actD in allActionsData)
        {
            allActions.Add(new ECActionM(actD));
        }
        availableActions = new List<ECActionM>();
        ongoingAction = null;
        ongoingEffect = null;

        //PFM: Modelos posibles de la casilla
        this.cellVariations = newViewCell.modelCell.cellVariations;

        //change model
        //this.viewCell.SwitchModel(newModelCell);

    }

    public void ApplyAction(ECActionM action)
    {
        ongoingAction = action;
        //CLARA en negativo porque est� en la BBDD en positivo y es el coste de la acci�n (resta siempre)
        ECModel.instance.ResourcesM.UpdateMoney(-action.actionD.moneyRequirement * action.actionD.turnDuration);

        cellState.Union(action.preStateChanges); //PFM: Se cambia el estado al iniciar la accion

        blocked = true;
        mapCell.BlockedFactor = 1;
        ongoingEffect = action.effectM;
    }

    //// Start is called before the first frame update
    //void Start()
    //{
    //    //TODO: instantiate visual cell content || JMPIZANA: for now visual content of a tile is fixed
    //}


    //TODO: [CLARA] 
    //add ongoingAction and ongoingEffects when the radial menu is selected
    //block cell

    public bool NextTurn(ref ResourcesData updateResources)
    {
        //ECController.instance.SelectCell(this);
        //ECController.instance.SelectCell(this);
        //ECModel.instance.SelectCell(this); //PFMARINAS: Sub-Model class must pass data to ModelManager, not directly to ControllerManager

        bool changeState = false; //pasa de bloqueada a desbloqueada

        if (ongoingAction != null)
        {
            ongoingEffect.UpdateEffect(ref updateResources); //PFM: Only one effect per action. Unique duration, no delay
            if (ongoingAction.UpdateAction())
            {
                //PFM: Se actualiza estado al terminar la accion
                cellState.Union(ongoingAction.postStateChanges);

                //if this action change the type of cell
                if (ongoingAction.actionD.changeCell)
                {
                    //ChangeCellInGrid(ongoingAction.actionD.changeCell, ongoingAction.actionD.changeModel);
                    ChangeCellInGrid(ongoingAction.actionD.changeCell);
                }
                //CLARA: esto era de la versi�n en la que no se activaban/desactivaban los modelos
                //else if (ongoingAction.actionD.changeModel)
                //{
                //    this.viewCell.SwitchModel(ongoingAction.actionD.changeModel);
                //}

                //Reset action
                ongoingAction.TurnCounter = 0;

                ongoingAction = null;
                ongoingEffect = null;
                blocked = false;
                mapCell.BlockedFactor = 0;
                changeState = true;

            }
        }


        //CLARA: Posible error de null transform desY SI YA NO EXISTE??????? 
        //es posible que haga el destroy antes??? si no no s� qu� puede pasar :_____(((

        UpdateCellVariations();
        //UpdateAvailableActions();

        return changeState;
    }

    public static HexCellType GetRandomType()
    {
        HexCellType[] cellTypes = (HexCellType[])System.Enum.GetValues(typeof(HexCellType));
        int chosenType = Random.Range(1, cellTypes.Length);//El primero es NONE, por eso no se coge
        return (HexCellType)cellTypes.GetValue(chosenType);
    }


    public void UpdateAvailableActions()
    {
        if (!blocked)
        {
            availableActions = new List<ECActionM>();
            foreach (ECActionM action in allActions)
            {
                if (action.CheckRequirements(this, ECModel.instance.ResourcesM))
                    availableActions.Add(action);
            }
        }
    }

    public bool HasAvailableActions()
    {
        return (availableActions != null && availableActions.Count > 0);
    }

    public List<ECActionM> getAvailableActions() { return availableActions; }

    //Cristian
    public void getOngoingEffects(ref ResourcesData globalEffect)
    {
        //PFM: Only one effect per action
        if (ongoingEffect != null)
        {
            globalEffect.money += ongoingEffect.getEffect().money;
            globalEffect.ecology += ongoingEffect.getEffect().ecology;
            globalEffect.business += ongoingEffect.getEffect().business;
            globalEffect.publicFavor += ongoingEffect.getEffect().publicFavor;
        }
    }

    //Looks at the conditions for each of the cell models and if they are met, makes that model visible
    public void UpdateCellVariations()
    {
        for (int i = 0; i < cellVariations.Length; i++)
        {
            StateValuePair stateCondition = cellVariations[i].state;

            int stateValue;
            if (cellState.TryGetValue(stateCondition.first.name, out stateValue))
            {
                if (stateValue == stateCondition.second)
                {
                    if (cellVariations[i].model == null)
                        Debug.Log(viewCell.name);
                    cellVariations[i].model.gameObject.SetActive(true);
                }
                else
                {
                      if (cellVariations[i].model == null)
                        Debug.Log(viewCell.name);
                    if (cellVariations[i].model == null) Debug.Log(viewCell.name);
                    cellVariations[i].model.gameObject.SetActive(false);
                }
            }
        }
    }
}


/*
//Swaps HexCellType of the HexCell with the HexCellType given by name
public void SwapHexCellType(string name)
{
    System.Array cellTypes = System.Enum.GetValues(typeof(HexCellType));
    for (int i = 0; i < cellTypes.Length; i++)
    {
        string cell = cellTypes.GetValue(i).ToString();
        if (string.Equals(cell, name))
        {
            //return (HexCellType)cellTypes.GetValue(i);
            cellType = (HexCellType)cellTypes.GetValue(i);
            return;
        }
    }

    cellType = HexCellType.None;
    //return HexCellType.None;
}
}
*/
