﻿using ECUtils;
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ECSaveDataM 
{
    public int seed;
    public int turn;
    public ECGlobalStateSaveDataM globalState;
    public ECCellSaveDataM[] cellsData;

    public ECSaveDataM(int seed, List<ECCellM> cells, ECResourcesM resources)
    {
        this.seed = seed;

        globalState = new ECGlobalStateSaveDataM(resources.GetResources(), resources.globalState);
        turn = ECController.instance.Turn;

        List<ECCellSaveDataM> cellsDataList  = new List<ECCellSaveDataM>();
        foreach(ECCellM cell in cells)
        {
            cellsDataList.Add(new ECCellSaveDataM(cell));
        }
        cellsData = cellsDataList.ToArray();
        
    }

}

[Serializable]
public class ECGlobalStateSaveDataM
{
    public float money, business, ecology, publicFavor;

    public StateStruct[] globalStateDict;

    public ECGlobalStateSaveDataM(ResourcesData resources, TotalStateDict dict)
    {
        money = resources.money;
        business = resources.business;
        ecology = resources.ecology;
        publicFavor = resources.publicFavor;

        if (dict != null)
        {
            //The ñapening
            List<StateStruct> svp = new List<StateStruct>();
            foreach (string key in dict.Keys)
            {
                int value = dict[key];
                svp.Add(new StateStruct(key, value));
            }

            globalStateDict = svp.ToArray();
        }

    }
}

[Serializable]
public class ECCellSaveDataM
{
    public int chosenType;
    public bool blocked;
    public StateStruct[] cellState;
    //MIGUEL TODO: Eliminar guardado de coordenadas al terminar el guardado
    public int X, Z; //X y Z se guardan para saber que celda es cual al debuguear.
    public ECCellOngoingActionSaveDataM ongoingAction;
    public ECCellOngoingEffectSaveDataM ongoingEffect; //PFM: Only one effect per action
    public int index;

    public ECCellSaveDataM(ECCellM cell)
    {
        chosenType = (int)cell.cellType;
        blocked = cell.blocked;

        if (cell.cellState != null)
        {
            List<StateStruct> svp = new List<StateStruct>();
            foreach (string key in cell.cellState.Keys)
            {
                int value = cell.cellState[key];
                svp.Add(new StateStruct(key, value));
            }

            cellState = svp.ToArray();

        }

        X = cell.mapCell.coordinates.X;
        Z = cell.mapCell.coordinates.Z;
        if (cell.OngoingAction != null)
            ongoingAction = new ECCellOngoingActionSaveDataM(cell.OngoingAction);

        //PFM: Only one effect per action
        if(cell.OngoingEffect != null)
            ongoingEffect = new ECCellOngoingEffectSaveDataM(cell.OngoingEffect);

        index = cell.idListCellsM;
    }
}

[Serializable]
public class ECCellOngoingActionSaveDataM
{
    public ECActionD actionData;
    public int turnCounter;

    public ECCellOngoingActionSaveDataM(ECActionM action)
    {
        actionData = action.actionD;
        turnCounter = action.TurnCounter;
    }
}

[Serializable]
public class ECCellOngoingEffectSaveDataM
{
    public ECEffectD effectData;

    public ECCellOngoingEffectSaveDataM(ECEffectM effect)
    {
        effectData = effect.effectD;
    }
}

[Serializable]
public struct StateStruct
{
    public string name;
    public int value;

    public StateStruct(string name, int value)
    {
        this.name = name;
        this.value = value;
    }
}
