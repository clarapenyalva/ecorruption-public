﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECUtils;
using System;

public class ECResourcesM
{
    public ResourcesData resources;

    //JMP: this should not be edited from the editor, but loaded from a 
    //StateValuePair list or array
    public TotalStateDict globalState;

    public float MaxValueResources { get { return maxValueResources; }  }
    public float PosValueChangeResources { get { return posValueChangeResources; } }
    public float NegValueChangeResources { get { return negValueChangeResources; } }

    public float PosValueChangeMoney { get { return posValueChangeMoney; } }
    public float NegValueChangeMoney { get { return negValueChangeMoney; } }
    public int Mult { get { return mult; } }


    /************************** PARAMETERS **************************/

    private int mult = 10;

    private float maxValueResources = 100.0f;

    //Neg (in abs) and pos range that will change resources
    private float posValueChangeResources = 2.0f;
    private float negValueChangeResources = 2.0f;

    //Neg (in abs) and pos range that will change money
    private float posValueChangeMoney = 10.0f;
    private float negValueChangeMoney = 7.0f;


    // Start is called before the first frame update
    public ECResourcesM()
    {
        resources.money = 40; 
        resources.ecology = 40;
        resources.business = 60;
        resources.publicFavor = 50;
    }

    public ResourcesData GetResources()
    {
        return resources;
    }

    public void UpdateMoney(float money)
    {
        resources.money += mult * money;
    }

    public void UpdateResources(ResourcesData effects)
    {
        float resourcesChangeMultiplier = ECModel.instance.resourcesChangeMultiplier;

        resources.money += mult * effects.money;
        resources.ecology += effects.ecology * resourcesChangeMultiplier;
        resources.business += effects.business * resourcesChangeMultiplier;
        resources.publicFavor += effects.publicFavor * resourcesChangeMultiplier;

        resources.money = Mathf.Clamp(resources.money, 0f, 100f);
        resources.ecology = Mathf.Clamp(resources.ecology, 0f, 100f);
        resources.business = Mathf.Clamp(resources.business, 0f, 100f);
        resources.publicFavor = Mathf.Clamp(resources.publicFavor, 0f, 100f);

        Debug.Log("Resources: " + "money: " + resources.money + 
            ", ecology:" + resources.ecology + ", business:" + resources.business +
            ", publicFavor:" + resources.publicFavor);
    }

}
