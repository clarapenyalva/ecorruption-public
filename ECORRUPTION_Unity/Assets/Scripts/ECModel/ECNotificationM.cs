﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Notification", menuName = "EC/Notification")]
public class ECNotificationM : ScriptableObject
{
    public string nameNotification = "";
    public string description = "";
    public List<ECEffectD> effects;
    //CLARA: quda introducir el sistema de Data-Model

}