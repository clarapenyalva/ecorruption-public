﻿using System.Collections.Generic;
using System.IO;

using UnityEngine;

class ECModel : MonoBehaviour
{
    public static ECModel instance;

    [HideInInspector] public ECController controller;

    public ECCellPrefabReferencesM referencesContainer;
    public HexMetrics hexMetrics;
    private ECResourcesM resourcesM;
    private ECSaveDataM saveData;

    public ECResourcesM ResourcesM { get { return resourcesM; } }
    public ECSaveDataM SaveData { get { return saveData; } }

    public List<ECCellM> cellsM;

    public float resourcesChangeMultiplier = 1f;

    public void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }

        controller = GetComponent<ECController>();
        resourcesM = new ECResourcesM();
        referencesContainer.Initialize();
    }

    public void UpdateResources(ResourcesData effects)
    {
        resourcesM.UpdateResources(effects);
    }

    public float getMaxValueSlidesResources()
    {
        return resourcesM.MaxValueResources;
    }
    public float getPosValueChangeResources()
    {
        return resourcesM.PosValueChangeResources;
    }
    public float getNegValueChangeResources()
    {
        return resourcesM.NegValueChangeResources;
    }
    public float getPosValueChangeMoney()
    {
        return resourcesM.PosValueChangeMoney;
    }
    public float getNegValueChangeMoney()
    {
        return resourcesM.NegValueChangeMoney;
    }

    public ResourcesData GetResources()
    {
        return resourcesM.GetResources();
    }

    //public void SelectCell(ECCellM cell)
    //{
    //    controller.SelectCell(cell);
    //}

    public void CreateCells(List<ECCellM> newCellsM)
    {
        cellsM = newCellsM;
    }

    public void UpdateRefCell(int id, ECCellM newCellM)
    {
        cellsM[id] = newCellM;
    }

    public List<ECCellM> GetCellsM() { return cellsM; }

    public void SetCellsM(List<ECCellM> newCellsM) { cellsM = newCellsM; }

    public void SaveGame()
    {
        saveData = new ECSaveDataM(controller.mapGenerator.seed, cellsM, resourcesM);
        string dataText = JsonUtility.ToJson(saveData, true);
        File.WriteAllText(Application.persistentDataPath + "/NEWJSON.json", dataText);

        PlayerPrefs.SetInt("HasSavedGame", 1);
    }

    public void LoadGame()
    {
        StreamReader reader = new StreamReader(Application.persistentDataPath + "/NEWJSON.json");
        string saveString = reader.ReadToEnd();
        reader.Close();

        saveData = JsonUtility.FromJson<ECSaveDataM>(saveString);
        controller.mapGenerator.seed = saveData.seed;
        ECController.instance.Turn = saveData.turn;
        resourcesM.globalState = new TotalStateDict(saveData.globalState.globalStateDict);

        resourcesM.resources.money = saveData.globalState.money;
        resourcesM.resources.business = saveData.globalState.business;
        resourcesM.resources.ecology = saveData.globalState.ecology;
        resourcesM.resources.publicFavor = saveData.globalState.publicFavor;
        controller.view.UpdateResources(resourcesM.resources);
    }

    public void DeleteGame()
    {
        string path = Application.persistentDataPath + "/NEWJSON.json";

        try
        {
            if (File.Exists(path))
                File.Delete(path);
        }
        catch (IOException ioExc)
        {
            Debug.Log(ioExc.Message);
        }

        PlayerPrefs.SetInt("HasSavedGame", 0);
    }
}