﻿using UnityEngine;

[System.Serializable, CreateAssetMenu(fileName = "NewDecision", menuName = "EC/Decision")]
public class ECDecisionM : ScriptableObject
{
    public string title;
    [TextArea(15, 20)]
    public string description;
    public Sprite icon;
    public ECDecisionOption[] options;
    public DecisionResourceInfluence
        ecoInfluence,
        businessInfluence,
        popularityInflucence;

    public enum DecisionResourceInfluence { NONE, DIRECT, INVERSE };
}

[System.Serializable]
public class ECDecisionOption
{
    /*JMP: los costes de las decisiones se comprenden en effectData al contrario que
     las acciones, ya que en este caso, los efectos son instantáneos y los datos serían redundantes.*/

    [TextArea(15, 20)]
    public string text;
    public ECEffectD effectData;
}