//[CreateAssetMenu(fileName = "new Effect", menuName = "EC/Efect")]
[System.Serializable]
public class ECEffectD
//: ScriptableObject
{
    //public string nameEffect = "";
    public float moneyChange = 0;
    public float populationChange = 0;
    public float ecologyChange = 0;
    public float businessChange = 0;
}
