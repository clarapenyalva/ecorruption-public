using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECUtils;

public class ECEffectM
{
    public ECEffectD effectD;

    public ECEffectM(ECEffectD data)
    {
        effectD = data;
    }

    public void UpdateEffect(ref ResourcesData updateResources)
    {
        // money, ecology, business, publicFavor
        updateResources.money += effectD.moneyChange;
        updateResources.ecology += effectD.ecologyChange;  
        updateResources.business += effectD.businessChange;
        updateResources.publicFavor += effectD.populationChange;
    }

    public ResourcesData getEffect()
    {
        ResourcesData effectList = new ResourcesData(0, 0, 0, 0);
        effectList.money = effectD.moneyChange;
        effectList.ecology = effectD.ecologyChange;
        effectList.business = effectD.businessChange;
        effectList.publicFavor = effectD.populationChange;
        return effectList;
    }
}