﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InGameMenuManager : MonoBehaviour
{
    public void ExitGame()
    {
        SceneManager.LoadScene(0);
    }

    public void CloseDial()
    {
        ECView.instance.OnClickOnMap(null);
        InputManager.instance.lockedCamera = true;
        InputManager.instance.enabled = false;
    }

    public void CloseInGameMenu()
    {
        InputManager.instance.lockedCamera = false;
        InputManager.instance.enabled = true;
    }
}
