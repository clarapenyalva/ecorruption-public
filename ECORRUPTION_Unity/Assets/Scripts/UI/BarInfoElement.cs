﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BarInfoElement : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public string selectedText;
    public BarInfoManager manager;

    public void OnPointerEnter(PointerEventData eventData)
    {
        manager.SelectedInfoElement(this);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        manager.DeselectInfoElement();
    }
}
