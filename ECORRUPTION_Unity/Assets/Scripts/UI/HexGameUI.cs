﻿using UnityEngine;

public class HexGameUI : MonoBehaviour {

	public HexGrid grid;
    public Color hexHighlightColor = Color.green;

    HexCell currentCell;

    //HexUnit selectedUnit;

    public void Initialize()
    {
        //JMP: TODO: investigate if these initializing instructions can be rellocated
        // so this class can be erased.

        //We disable this way fog of war
        Shader.EnableKeyword("HEX_MAP_EDIT_MODE");
        enabled = true;
        grid.ShowUI(true);
        grid.ClearPath();
    }

 //   public void SetEditMode (bool toggle) { //TODO: siempre a true
	//	enabled = !toggle;
	//	grid.ShowUI(!toggle);
	//	grid.ClearPath();
	//	if (toggle) {
	//		Shader.EnableKeyword("HEX_MAP_EDIT_MODE");// Esto hará lo de la niebla   
	//	}
	//	else {
	//		Shader.DisableKeyword("HEX_MAP_EDIT_MODE");
	//	}
	//}

	//void Update () {
	//	if (!EventSystem.current.IsPointerOverGameObject()) {
	//		if (Input.GetMouseButtonDown(0)) {
	//			DoSelection();//[JMPIZANA]: actions on click
 //           }
	//		//else if (selectedUnit) {
	//		//	if (Input.GetMouseButtonDown(1)) {
	//		//		DoMove();
	//		//	}
	//		//	else {
	//		//		DoPathfinding();
	//		//	}
	//		//}
	//	}
	//}

	//void DoPathfinding () {
	//	if (UpdateCurrentCell()) {
	//		if (currentCell && selectedUnit.IsValidDestination(currentCell)) {
	//			grid.FindPath(selectedUnit.Location, currentCell, selectedUnit);
	//		}
	//		else {
	//			grid.ClearPath();
	//		}
	//	}
	//}

	//void DoMove () {
	//	if (grid.HasPath) {
	//		selectedUnit.Travel(grid.GetPath());
	//		grid.ClearPath();
	//	}
	//}
}