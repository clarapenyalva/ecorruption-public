﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BarInfoManager : MonoBehaviour
{

    public TextMeshProUGUI infoText;

    public void SelectedInfoElement(BarInfoElement element)
    {
        infoText.text = element.selectedText;
        infoText.gameObject.SetActive(true);
    }

    public void DeselectInfoElement()
    {
        infoText.gameObject.SetActive(false);
    }
}
