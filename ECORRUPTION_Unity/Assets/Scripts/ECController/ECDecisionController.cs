﻿using ECUtils;
using System.Collections.Generic;
using UnityEngine;

public class ECDecisionController : MonoBehaviour
{
    public static ECDecisionController instance;

    private PonderedList<ECDecisionM> decisionsPonderedList;

    [HideInInspector] public bool deciding = false;

    [Range(0f, 1f)]
    public float chanceIncreasePerTurn = .15f;
    private float decisionChance = 0.0f;

    public int initialTurnsWithoutDecisions = 3;
    private int turnTimer = 0;

    public ECDecisionsContainer decisionsContainer;
    public ECDecisionV decView;

    public void Awake()
    {
        if (instance != null && instance != this) Destroy(gameObject);
        else instance = this;

        //decisionsPonderedList.Recalculate();
        decisionsPonderedList = new PonderedList<ECDecisionM>(decisionsContainer.decisions, null);

        turnTimer = 0;
    }

    public void NewTurn()
    {
        turnTimer++;

        decisionChance += chanceIncreasePerTurn;

        if (turnTimer < initialTurnsWithoutDecisions) return;

        if (Random.Range(0f, 1f) < decisionChance)
        {
            decisionChance = 0f;
            StartDecision();
        }

        //Debug.Log("Next Decision Chance: " + decisionChance);

    }

    private void StartDecision()
    {
        UpdateWeights();

        ECDecisionM selectedDecision = decisionsPonderedList.GetRandom();

        if (selectedDecision == null)
        {
            return;//JMP: si no se encuentra decision disponible, no se hace una decision
        }
        else
        {
            decView.StartDecision(selectedDecision);
            deciding = true;
            //JMP: informar al controlador global de juego de que la interacción fuera de la 
            //decisión debe estar desactivada.
            InputManager.instance.enabled = false; //JMP: Oooo podríamos saltarnos el controller 😈
        }       
    }

    private void UpdateWeights()
    {
        float ecoInfluence, bussInfluence, populInfluence, newWeight;
        for (int i = 0; i < decisionsPonderedList.Count; i++)
        {
            ECDecisionM decision = decisionsPonderedList.GetValue(i);

            ecoInfluence = ECModel.instance.ResourcesM.resources.ecology / 100f;
            ecoInfluence = GetInfluence(decision.ecoInfluence, ecoInfluence);

            bussInfluence = ECModel.instance.ResourcesM.resources.business / 100f;
            bussInfluence = GetInfluence(decision.businessInfluence, bussInfluence);

            populInfluence = ECModel.instance.ResourcesM.resources.publicFavor / 100f;
            populInfluence = GetInfluence(decision.popularityInflucence, populInfluence);

            newWeight = ecoInfluence + bussInfluence + populInfluence;

            decisionsPonderedList.SetWeight(i, newWeight);
        }
    }

    private float GetInfluence(ECDecisionM.DecisionResourceInfluence influenceMode, float normalizedValue)
    {
        switch (influenceMode)
        {
            case ECDecisionM.DecisionResourceInfluence.NONE:
                return .5f;

            case ECDecisionM.DecisionResourceInfluence.DIRECT:
                return normalizedValue;

            case ECDecisionM.DecisionResourceInfluence.INVERSE:
                return 1f - normalizedValue;
        }
        return .5f;
    }

    public void ChoseDecision(ECDecisionOption chosenOption)
    {
        ResourcesData resChanges = new ResourcesData(0f, 0f, 0f, 0f);
        ECEffectM effect = new ECEffectM(chosenOption.effectData);
        effect.UpdateEffect(ref resChanges);
        ECController.instance.UpdateResources(resChanges);

        //JMP: en el futuro aquí podrían haber cambios de estado, de casilla, etc.
        // ...
        //-----

        ECController.instance.UpdateAvailableActions();

        deciding = false;
        InputManager.instance.enabled = true;
    }
}
