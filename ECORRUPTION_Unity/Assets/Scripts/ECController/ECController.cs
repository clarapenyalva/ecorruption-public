﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

class ECController : MonoBehaviour
{
    private const int ecoWinScene = 2;
    private const int ecoLoseScene = 3;

    private const int bizWinScene = 4;
    private const int bizLoseScene = 5;

    private const int popWinScene = 6;
    private const int popLoseScene = 7;


    public static ECController instance;

    public HexGrid grid;
    public HexMapGenerator mapGenerator;
    public HexGameUI hexUI;
    public int mapSizeX = 40, mapSizeZ = 30;
    public bool wrapping = false;
    public int Turn
    {
        get { return turn; }
        set { turn = value; }
    }
    //public bool loadFromSave = false;

    [HideInInspector] public ECModel model;
    [HideInInspector] public ECView view;

    [HideInInspector] public MusicSOController musicC;

    private int turn;
    private String winBar;


    //public void SetLoadFromSave(bool value)
    //{
    //    loadFromSave = value;
    //}
    public void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
        InitializeAwake();
    }

    private void InitializeAwake()
    {
        model = GetComponent<ECModel>();
        view = GetComponent<ECView>();
        musicC = GetComponent<MusicSOController>();
        turn = 0;
    }

    private void Start()
    {
        //musicC.PlayMusic("ingame");
        ResourcesData resources = model.GetResources();
        view.UpdateResources(resources);
        // Actualizar contaminacion estetica
        view.camRig.GetComponentInChildren<PollutionBehaviour>().ApplyPollution(resources.ecology);

        grid.Initialize();
        mapGenerator.GenerateMap(mapSizeX, mapSizeZ, wrapping);
        hexUI.Initialize();
        HexMapCamera.ValidatePosition();

        //JMP: we need to initialize each cell's actions so selection can be correctly managed
        foreach (ECCellM cellM in model.GetCellsM())
        {
            cellM.UpdateAvailableActions();
        }
    }

    public void UpdateResources(ResourcesData effects)
    {
        model.UpdateResources(effects);
        // Actualizar contaminacion estetica
        view.camRig.GetComponentInChildren<PollutionBehaviour>().ApplyPollution(model.ResourcesM.resources.ecology);

        ResourcesData resources = model.GetResources();
        view.UpdateResources(resources);

        // Check Endgame
        if (fullBar(resources))
        {
            Debug.Log("You win! Obtained the " + winBar + " victory!");
        }
        else if (emptyBar(resources))
        {
            Debug.Log("GAME OVER");
        }
    }

    //Cristian
    public void UpdateGlobalResources(ResourcesData globalEffects)
    {
        view.UpdateGlobalResources(globalEffects);
    }

    public void NextTurn()
    {
        // money, ecology, business, publicFavor
        //musicC.PlayMusic("menu");
        //efectos repasando el mapa
        ResourcesData effects = new ResourcesData(0, 0, 0, 0);

        for (int i = 0; i < model.cellsM.Count; i++)
        {
            ECCellM cellM = model.cellsM[i];

            if (cellM.NextTurn(ref effects))
                view.CompletedAction(cellM.viewCell); //TODO
        }

        UpdateResources(effects);


        //Cristian: update effects
        ResourcesData globalEffects = new ResourcesData(0, 0, 0, 0);
        foreach (ECCellM cellM in model.GetCellsM())
        {
            cellM.getOngoingEffects(ref globalEffects);
        }

        UpdateGlobalResources(globalEffects);

        turn++;
        Debug.Log("---------Turno: " + turn + "---------");
        Debug.Log("Resources - Money:" + model.ResourcesM.GetResources().money +
                    ". Popularidad: " + model.ResourcesM.GetResources().publicFavor +
                    ". Eco: " + model.ResourcesM.GetResources().ecology +
                    ". Empresas: " + model.ResourcesM.GetResources().business);
        //view.UpdateView(); //JMP -> Clara: Esto no debería estar sin comentar?

        //JMP: update decision system
        ECDecisionController.instance.NewTurn();

        UpdateAvailableActions();

        model.SaveGame();
    }

    public void UpdateAvailableActions()
    {
        //Se contabilizan las acciones disponibles para el siguiente turno
        //a partir del estado global actualizado de este turno
        for (int i = 0; i < model.cellsM.Count; i++)
        {
            ECCellM cellM = model.cellsM[i];
            cellM.UpdateAvailableActions();
        }
    }

    public void SelectCell(ECCellM cell)
    {
        //JMP: Update availableactions is taken care of in the cell.nextTurn function
        //(executed in ECController.NextTurn())
        //cell.UpdateAvailableActions();
    }

    private bool fullBar(ResourcesData resources)
    {
        bool won = false;
        int sceneToLoad = -1;
        if (resources.ecology >= 100)
        {
            winBar = "ECOLOGY";
            sceneToLoad = ecoWinScene;
            won = true;
        }
        else if (resources.business >= 100)
        {
            winBar = "BUSINESS";
            sceneToLoad = bizWinScene;
            won = true;
        }
        else if (resources.publicFavor >= 100)
        {
            winBar = "PUBLIC FAVOR";
            sceneToLoad = popWinScene;
            won = true;
        }

        if (won)
        {
            model.DeleteGame();
            SceneManager.LoadScene(sceneToLoad);
        }

        return won;
    }

    private bool emptyBar(ResourcesData resources)
    {
        bool lost = false;
        int sceneToLoad = -1;
        if (resources.ecology <= 0)
        {
            lost = true;
            sceneToLoad = ecoLoseScene;

        }
        else if (resources.business <= 0)
        {
            lost = true;
            sceneToLoad = bizLoseScene;
        }
        else if (resources.publicFavor <= 0)
        {
            lost = true;
            sceneToLoad = popLoseScene;
        }

        if (lost)
        {
            model.DeleteGame();
            SceneManager.LoadScene(sceneToLoad);
        }

        return lost;
    }
}
