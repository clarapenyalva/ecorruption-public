﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/
using System.Collections.Generic;

public abstract class IParams
{

}
public class RequireStruct<T> where T : struct { }
public class RequireClass<T> where T : class { }

public struct ResourcesData
{
    public float money;
    public float ecology;
    public float business;
    public float publicFavor;

    public ResourcesData(float m, float e, float b, float p)
    {
        money = m;
        ecology = e;
        business = b;
        publicFavor = p;
    }
}

