﻿[System.Serializable]
public class Pair<T1, T2>//TODO: why do I have this when Tuple<> exists?? --> Maybe serialization
{
    public T1 first;
    public T2 second;

    public Pair(T1 first, T2 second)
    {
        this.first = first;
        this.second = second;
    }
}

[System.Serializable]
public abstract class Triplet<T1, T2, T3>//TODO: why do I have this when Tuple<> exists?? --> Maybe serialization
{
    public T1 first;
    public T2 second;
    public T3 third;

    public Triplet(T1 first, T2 second, T3 third)
    {
        this.first = first;
        this.second = second;
        this.third = third;
    }
}


[System.Serializable]
public class StateValuePair : Pair<ECStateM, int>
{
    public StateValuePair() : base(null, -1) { }
    public StateValuePair(ECStateM first, int second) : base(first, second)
    {
    }
}
