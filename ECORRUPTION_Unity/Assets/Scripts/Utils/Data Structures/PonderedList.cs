﻿using System.Collections.Generic;

namespace ECUtils
{
    [System.Serializable]
    public class PonderedList<T>
    {
        public Pair<T, float>[] valuesAndWeights;
        public T defaultValue;

        bool initialized = false;
        float totalWeight = 0f;

        //public abstract void Initialize(List<T> list, float initialWeight);

        public PonderedList(List<T> list, T defaultValue)
        {
            Initialize(list);
            this.defaultValue = defaultValue;
        }

        public void Initialize(List<T> list, float initialWeight = 0f)
        {
            totalWeight = initialWeight * list.Count;

            List<Pair<T, float>> tempList = new List<Pair<T, float>>(list.Count);
            for (int i = 0; i < list.Count; i++)
                tempList.Add(new Pair<T, float>(list[i], initialWeight));

            valuesAndWeights = tempList.ToArray();
            initialized = true;
        }

        public void Recalculate()
        {
            totalWeight = 0f;
            for (int i = 0; i < valuesAndWeights.Length; i++)
            {
                totalWeight += valuesAndWeights[i].second;
            }
            initialized = true;
        }

        public T GetRandom()
        {
            if (!initialized) Recalculate();

            float selected = UnityEngine.Random.Range(0f, (float)totalWeight);

            float currentW = 0f;
            for (int i = 0; i < valuesAndWeights.Length; i++)
            {
                currentW += valuesAndWeights[i].second;
                if (currentW > selected)
                {
                    return valuesAndWeights[i].first;
                }
            }

            return defaultValue;
        }

        public T GetRandom(float chanceOfNoCell)
        {
            if (chanceOfNoCell > UnityEngine.Random.Range(0f, 1f)) return defaultValue;

            return GetRandom();
        }

        public float GetWeight(int index)
        {
            return valuesAndWeights[index].second;
        }

        public void SetWeight(int index, float value)
        {
            float difference = -valuesAndWeights[index].second + value;
            totalWeight += difference;
            valuesAndWeights[index].second = value;
        }

        public T GetValue(int index)
        {
            return valuesAndWeights[index].first;
        }

        public void SetValue(int index, T value)
        {
            valuesAndWeights[index].first = value;
        }

        public int Count { get { return valuesAndWeights.Length; } }

    }
}