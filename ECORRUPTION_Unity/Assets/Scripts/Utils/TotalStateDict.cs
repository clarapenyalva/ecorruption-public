﻿using System.Collections.Generic;

//JMP: changed this because having a dictionary with a class as a key was being problematic
//[System.Serializable]
//public class TotalStateDict : SerializableDictionary<ECStateM, int>
//{ }

/// <summary>
/// JMP:
/// This class is not designed to be edited from the editor, but to be loaded from a
/// StateValuePair list or array. Make it public at your own risk.
/// </summary>
[System.Serializable]
public class TotalStateDict : SerializableDictionary<string, int>
{
    //JMP: remember that base func is always called before class func
    public TotalStateDict(StateValuePair[] initializerArray)
        : base(initializerArray != null ? initializerArray.Length : 0)
    {
        for (int i = 0; i < initializerArray.Length; i++)
        {
            Add(initializerArray[i].first.nameState, initializerArray[i].second);
        }
    }

    //JMP: remember that base func is always called before class func
    public TotalStateDict(List<StateValuePair> initializerArray) : base(initializerArray.Count)
    {
        for (int i = 0; i < initializerArray.Count; i++)
        {
            Add(initializerArray[i].first.nameState, initializerArray[i].second);
        }
    }

    //MIGUEL: Inicializacion con StateStruct para el cargado desde JSON
    public TotalStateDict(StateStruct[] initializerArray)
        : base(initializerArray != null ? initializerArray.Length : 0)
    {
        for (int i = 0; i < initializerArray.Length; i++)
        {
            Add(initializerArray[i].name, initializerArray[i].value);
        }
    }
}