﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ECUtils.AI.FSM
{
    public abstract class FSMDecision<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType> : ScriptableObject
        where ControllerType : FSMController<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where StateType : FSMState<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where OngoingActionType : FSMAction<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where InstantActionType : FSMAction<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where TransitionType : FSMTransition<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where DecisionType : FSMDecision<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
    {
        public bool successIs = true;
        internal bool AssertExternal(ControllerType controller)
        {
            bool result = Assert(controller);
            //if(!((successIs && result) || (!successIs && !result)))
             //   Debug.Log(name + " decision failed. || SucessIs: " + successIs + " || Result: " + result);


            return (successIs && result) || (!successIs && !result);
        }

        protected abstract bool Assert(ControllerType controller);
    }
}
