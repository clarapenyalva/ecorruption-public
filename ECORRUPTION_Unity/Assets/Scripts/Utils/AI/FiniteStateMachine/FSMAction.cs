﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
 * 
 * TODO: differentiate between one-shot actions and continuous actions
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ECUtils.AI.FSM
{
    public abstract class FSMAction<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType> : ScriptableObject
        where ControllerType : FSMController<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where StateType : FSMState<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where OngoingActionType : FSMAction<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where InstantActionType : FSMAction<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where TransitionType : FSMTransition<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where DecisionType : FSMDecision<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
    {
        public abstract void Act(ControllerType controller, float deltaTime);
    }
}

