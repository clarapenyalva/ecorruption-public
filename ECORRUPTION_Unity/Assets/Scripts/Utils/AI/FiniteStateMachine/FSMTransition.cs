﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ECUtils.AI.FSM
{
    [System.Serializable]
    public abstract class FSMTransition<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType> //: ScriptableObject
       where ControllerType : FSMController<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where StateType : FSMState<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where OngoingActionType : FSMAction<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where InstantActionType : FSMAction<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where TransitionType : FSMTransition<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
        where DecisionType : FSMDecision<ControllerType, StateType, OngoingActionType, InstantActionType, TransitionType, DecisionType>
    {
        public string name;
        public StateType nextstate;
        public DecisionType[] decisions;
        public InstantActionType[] actions;

        public bool Assert(ControllerType controller)
        {
            if (decisions.Length == 0) return false;

            //Debug.Log("Asserting " + name + " transition");

            bool resultSuccessful = true;
            for (int i = 0; i < decisions.Length; i++)
            {
                if (!decisions[i].AssertExternal(controller))
                {
                    resultSuccessful = false;
                    break;
                }
            }

            if (resultSuccessful)
            {
#if UNITY_EDITOR
                if (resultSuccessful && controller.debugMessages)
                    Debug.Log("FSMTransition: Transition " + name + " was successful.");
                //else Debug.Log("FSMTransition: Transition " + name + " was NOT successful.");
#endif
                for (int j = 0; j < actions.Length; j++)
                {
                    actions[j].Act(controller, Time.deltaTime);
                }
                controller.SetState(nextstate);
            }



            return resultSuccessful;
        }
    }
}
