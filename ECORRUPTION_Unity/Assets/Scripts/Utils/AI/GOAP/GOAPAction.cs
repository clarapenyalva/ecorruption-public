﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace ECUtils.AI.GOAP
{
    /// <summary>
    /// REMEMBER AN ACTION DOES NOT DEPEND OF A CHARACTER
    /// </summary>
    [Serializable]
    public abstract class GOAPAction : ScriptableObject
    {
        [SerializeField] protected float baseCost = float.NaN;

        [SerializeField]
        GOAPState preconditions;

        [SerializeField]
        GOAPState effects;

        //public bool requiresInRange = true;
        //[HideInInspector] public bool inRange = false;

        /// <summary>
        /// Target of the action. Can be null or dynamic.
        /// </summary>
        //public GameObject target;

        /// <summary>
        /// Asserts the set preconditions AND also maybe procedural ones
        /// </summary>
        /// <returns></returns>
        public abstract bool AssertPreconditions(GOAPAgent agent, out Transform potentialTarget);

        /// <summary>
        /// Summary of the effect that this action takes into the world
        /// </summary>
        public abstract GOAPState ChangeWorldState(GOAPState previousWorldState);

        /// <summary>
        /// Calculates the cost of the action, procedurally if necessary
        /// </summary>
        public abstract float CalculateCost(GOAPAgent agent, Vector3 virtualAgentPosition, Vector3 virtualTargetPosition);

        /// <summary>
        /// Manipulations done when the Agent enters this action from another
        /// </summary>
        public abstract void EnterAction(GOAPAgent agent);
        
        /// <summary>
        /// Manipulations done when the Agent exits this action to another
        /// </summary>
        public abstract void ExitAction(GOAPAgent agent);

        /// <summary>
        /// Changes to the world done in time in a many-frame scale.
        /// It will call all necessary agent functions to do so.
        /// </summary>
        /// <param name="agent"></param>
        public abstract void Perform(GOAPAgent agent, float deltaTime);

        /// <summary>
        /// Asserts if the action has been completed in the world
        /// </summary>
        /// <returns></returns>
        public abstract bool IsComplete(GOAPAgent agent);

        /// <summary>
        /// Checks if an agent is in range for the action
        /// </summary>
        /// <returns></returns>
        public abstract bool CheckInRange(GOAPAgent agent, GameObject target);

        //NOTE: functions from here on directly added from https://github.com/sploreg/goap, 
        //which is the main information source for this work

        public void AddPrecondition(string key, bool value)
        {
            preconditions.Add(key, value);
        }

        public void RemovePrecondition(string key)
        {
            preconditions.RemoveSubstate(key);
        }

        public void AddEffect(string key, bool value)
        {
            effects.Add(key, value);
        }

        public void RemoveEffect(string key)
        {
            effects.RemoveSubstate(key);
        }

        public GOAPState Preconditions
        {
            get
            {
                return preconditions;
            }
        }

        public GOAPState Effects
        {
            get
            {
                return effects;
            }
        }
    }
}
