﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace ECUtils.AI.GOAP
{
    public static class GOAPPlanner
    {
        /// <summary>
        /// Returns null if no plan could be found
        /// </summary>
        /// <param name="agent"></param>
        /// <param name="currentWorldState"></param>
        /// <param name="goalWorldState"></param>
        /// <returns></returns>
        public static Queue<KeyValuePair<GOAPAction, Transform>> Plan(GOAPAgent agent, GOAPAction[] availableActions,
                                        GOAPState currentWorldState,
                                        GOAPState goalWorldState)
        {
            List<KeyValuePair<GOAPAction, Transform>> usableActionsAndTargets =
                new List<KeyValuePair<GOAPAction, Transform>>();

            for (int i = 0; i < availableActions.Length; i++)
            {
                Transform potentialTarget; //we redeclare each iteration to unbind from previous actions
                if (availableActions[i].AssertPreconditions(agent, out potentialTarget))
                {
                    usableActionsAndTargets.Add(new KeyValuePair<GOAPAction, Transform>(availableActions[i], potentialTarget));
                }
            }

            List<GOAPNode> leaves = new List<GOAPNode>();

            GOAPNode start = new GOAPNode(null, 0, currentWorldState, null, null);

            Vector3 agentVirtualPosition = agent.transform.position;
            bool success = BuildGraph(start, ref leaves, usableActionsAndTargets, goalWorldState, agent, ref agentVirtualPosition);

            if (!success)
            {
                // oh no, we didn't get a plan
                Debug.Log("NO PLAN");
                return null;
            }

            // get the cheapest leaf
            GOAPNode cheapest = null;
            foreach (GOAPNode leaf in leaves)
            {
                if (cheapest == null)
                    cheapest = leaf;
                else
                {
                    if (leaf.GetCurrentCost() < cheapest.GetCurrentCost())
                        cheapest = leaf;
                }
            }

            // get its node and work back through the parents
            List<KeyValuePair<GOAPAction, Transform>> result = new List<KeyValuePair<GOAPAction, Transform>>();
            GOAPNode n = cheapest;
            while (n != null)
            {
                if (n.action != null)
                {
                    result.Insert(0, new KeyValuePair<GOAPAction, Transform>(n.action, n.potentialTarget)); // insert the action in the front
                }
                n = n.currentParent;
            }
            // we now have this action list in correct order

            Queue<KeyValuePair<GOAPAction, Transform>> queue = new Queue<KeyValuePair<GOAPAction, Transform>>();
            foreach (KeyValuePair<GOAPAction, Transform> a in result)
            {
                queue.Enqueue(a);
            }

            // hooray we have a plan!
            return queue;
        }

        private static bool BuildGraph(GOAPNode parent, ref List<GOAPNode> leaves,
            List<KeyValuePair<GOAPAction, Transform>> usableActionsAndTargets,
            GOAPState goalWorldState, GOAPAgent agent, ref Vector3 virtualCharacterPosition)
        {
            //TODO: badly implemented, not A*
            //ALSO IT DOES NOT CHECK FOR POSSIBLE REPEATING ACTIONS
            bool goalFound = false;
            GOAPAction action;
            for (int i = 0; i < usableActionsAndTargets.Count; i++)
            {
                action = usableActionsAndTargets[i].Key;
                if (action.Preconditions.CheckStateInConditions(parent.state))
                {
                    GOAPState currentState = parent.state.AddToStateVirtual(action.Effects);

                    GOAPNode node = new GOAPNode(parent,
                        parent.GetCurrentCost() + action.CalculateCost(agent,
                                                    virtualCharacterPosition,
                                                    usableActionsAndTargets[i].Value.position
                                                    ),
                        currentState, action, usableActionsAndTargets[i].Value);

                    if (goalWorldState.CheckConditionsInState(currentState))
                    {
                        //Solution found
                        leaves.Add(node);
                        goalFound = true;
                    }
                    else
                    {
                        List<KeyValuePair<GOAPAction, Transform>> subset = 
                            ActionSubset(usableActionsAndTargets, action);

                        Vector3 newVirtualPosition = usableActionsAndTargets[i].Value.position;

                        bool found = BuildGraph(node, ref leaves, subset, goalWorldState, agent, 
                            ref newVirtualPosition);
                        if (found) goalFound = true;
                    }
                }
            }

            return goalFound;
        }

        private static List<KeyValuePair<GOAPAction, Transform>> ActionSubset(List<KeyValuePair<GOAPAction, Transform>> usableActions, 
            GOAPAction actionToPrune)
        {
            List<KeyValuePair<GOAPAction, Transform>> subset = new List<KeyValuePair<GOAPAction, Transform>>();

            for (int i = 0; i < usableActions.Count; i++)
            {
                if (!usableActions[i].Key.Equals(actionToPrune))
                    subset.Add(usableActions[i]);
            }

            return subset;
        }
    }

    public class GOAPNode
    {
        //A node is identified by 
        //Action that contains
        //depth from which it was accessed (Actions from origin) 

        /// <summary>
        /// Node from which this one was accessed with lesser cost
        /// </summary>
        public GOAPNode currentParent;

        public GOAPState state;
        public GOAPAction action;

        float currentCost = float.PositiveInfinity;
        bool visited = false;
        public int depth = int.MinValue;
        public Transform potentialTarget;

        public GOAPNode(GOAPNode currentParent, float currentCost, GOAPState nodeState, GOAPAction action, Transform potentialTarget)
        {
            this.currentParent = currentParent;
            this.currentCost = currentCost;
            this.state = nodeState;
            this.action = action;
            this.potentialTarget = potentialTarget;
        }
        

        public float GetCurrentCost()
        {
            return currentCost;
        }

        public void SetCurrentCost(float value)
        {
            currentCost = value;
        }

        //A node is identified by 
        //Action that contains
        //depth from which it was accessed (Actions from origin) 
        public override bool Equals(object other)
        {
            GOAPNode otherNode = other as GOAPNode;
            return (depth == otherNode.depth)
                && (action == otherNode.action) //Note: we are comparing action REFERENCE, so it has to be the same action
                ;
        }

        //Automatically done
        public override int GetHashCode()
        {
            var hashCode = -1418859703;
            hashCode = hashCode * -1521134295 + EqualityComparer<GOAPNode>.Default.GetHashCode(currentParent);
            hashCode = hashCode * -1521134295 + EqualityComparer<GOAPState>.Default.GetHashCode(state);
            hashCode = hashCode * -1521134295 + EqualityComparer<GOAPAction>.Default.GetHashCode(action);
            hashCode = hashCode * -1521134295 + currentCost.GetHashCode();
            hashCode = hashCode * -1521134295 + visited.GetHashCode();
            hashCode = hashCode * -1521134295 + depth.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<Transform>.Default.GetHashCode(potentialTarget);
            return hashCode;
        }
    }
}
