﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class GOAPState
{
    [Serializable]
    public class GOAPSubState: SerializableDictionary<string, bool>
    {
        public GOAPSubState()
        {
        }

        public GOAPSubState(int count) : base(count)
        {

        }
    }

    [SerializeField]
    GOAPSubState substates;

    //Dictionary<string, object> substates;
    public GOAPState()
    {
        substates = new GOAPSubState();
    }

    public GOAPState(Dictionary<string, bool> substates)
    {
        this.substates = new GOAPSubState(substates.Count);
        foreach (KeyValuePair<string, bool> substate in substates)
        {
            this.substates.Add(substate.Key, substate.Value);
        }
    }

    public void Add(string key, bool value)
    {
        substates.Add(key, value);
    }

    //public bool CheckConditionsInState(GOAPState conditions)
    //{
    //    bool allMatch = true;
    //    foreach (KeyValuePair<string, object> substate in substates)
    //    {
    //        bool match = false;
    //        foreach (KeyValuePair<string, object> condition in conditions.SubStates)
    //        {
    //            if (substate.Equals(condition))
    //            {
    //                match = true;
    //                break;
    //            }
    //        }
    //        if (!match)
    //        {
    //            allMatch = false;
    //            break;
    //        }
    //    }

    //    return allMatch;
    //}

    public bool CheckConditionsInState(GOAPState conditions)
    {
        bool allMatch = true;
        Dictionary<string, bool> conditionStates = conditions.SubStates;

        foreach (KeyValuePair<string, bool> condition in conditionStates)
        {
            try
            {
                allMatch = (substates[condition.Key] == condition.Value);

                if (!allMatch) break;
            }
            catch (KeyNotFoundException e)
            {
                //The key was not in this state
                allMatch = false;
                break;
            }
        }

        return allMatch;
    }

    public bool CheckStateInConditions(GOAPState conditions)
    {
        bool allMatch = true;
        Dictionary<string, bool> conditionStates = conditions.SubStates;

        foreach (KeyValuePair<string, bool> substate in substates)
        {
            try
            {
                allMatch = (conditionStates[substate.Key] == substate.Value);

                if (!allMatch) break;
            }
            catch (KeyNotFoundException e)
            {
                //The key was not in this state
                allMatch = false;
                break;
            }
        }

        return allMatch;
    }

    public void AddToState(GOAPState changes)
    {
        foreach (KeyValuePair<string, bool> change in changes.substates)
        {
            try
            {
                substates.Add(change.Key, change.Value);
            }
            catch (ArgumentException e)
            {
                //https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.dictionary-2.add?view=netframework-4.7.2
                //In the case the element already exists in the dictionary
                substates[change.Key] = change.Value;
            }
        }
    }

    /// <summary>
    /// Used when checking pathfinding in the graph
    /// </summary>
    /// <param name="changes"></param>
    /// <returns></returns>
    public GOAPState AddToStateVirtual(GOAPState changes)
    {
        GOAPState result = new GOAPState(changes.SubStates);

        result.AddToState(changes);

        return result;
    }

    public Dictionary<string, bool> SubStates
    {
        get { return substates; }
    }

    public void RemoveSubstate(string key)
    {
        substates.Remove(key);
    }

    public string PrettyPrint()
    {
        String s = "";
        foreach (KeyValuePair<string, bool> kvp in substates)
        {
            s += kvp.Key + ":" + kvp.Value.ToString();
            s += ", ";
        }
        return s;
    }

    public bool this[string key]
    {
        get { return substates[key]; }
        set { substates[key] = value; }
    }
}
