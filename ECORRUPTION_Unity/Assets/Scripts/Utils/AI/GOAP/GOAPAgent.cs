﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using ECUtils.AI.FSM;

namespace ECUtils.AI.GOAP
{
    [RequireComponent(typeof(IFSMController))]
    public abstract class GOAPAgent : MonoBehaviour
    {
        IFSMController fsmController;

        GOAPState goalWorldState;

        public GOAPState[] availableGoals;
        public GOAPAction[] availableActions; //Actions should be loaded in first place from editor

        [Range(0f, 10f), Tooltip("Seconds that the agent is inactive if no goal is achievable.")]
        public float inactiveSeconds = 1f;

        float actionStartTime = 0f;

        public float distanceCostMultiplier = 0.5f;

        /// <summary>
        /// Target of the agent in a given action. Can be null or dynamic.
        /// </summary>
        public GameObject target;

        Queue<KeyValuePair<GOAPAction, Transform>> actionPlan; //Actions that need to be performed in the future
        [SerializeField] GOAPAction currentAction; //Action currently being performed
        bool active = true;
        public bool currentTargetNull = true;

        [Header("Debug")]
        public bool debugMessages = false;

        protected virtual void Awake()
        {
            fsmController = GetComponent<IFSMController>();

            //InitializeGoals();
        }

        //protected abstract void InitializeGoals();

        protected virtual void Start()
        {

        }

        protected abstract void Update();

        public void Perform(float deltaTime)
        {
            if (currentAction == null) return;

            //We check if the target has been lost
            if (target == null && !currentTargetNull)
            {
                //The target has been lost, replan
                PrepareRePlan();
                return;
            }

            currentAction.Perform(this, deltaTime);

            if (currentAction.IsComplete(this))
            {
                currentAction.ExitAction(this);

                if (actionPlan != null && actionPlan.Count > 0)
                {
                    KeyValuePair<GOAPAction, Transform> currentCombination = actionPlan.Dequeue();
                    currentAction = currentCombination.Key;

                    if (currentCombination.Value == null)
                        currentTargetNull = true;
                    else
                        target = currentCombination.Value.gameObject;

                    currentAction.EnterAction(this);
                }
                else
                {
                    PrepareRePlan();
                }
            }
        }

        public void PrepareRePlan()
        {
            actionPlan = null;
            currentAction = null;
        }

        public void Plan()
        {
            //TODO: NOTE: for now this is synchronous code (which is extremely inneficient) 
            //This should be written with async code in the future

            if (!active) return;

            HashSet<GOAPState> candidateGoals = new HashSet<GOAPState>(availableGoals);

            GOAPState currentWorldState = GetCurrentWorldState();

            actionPlan = null;
            while (candidateGoals.Count > 0 && actionPlan == null)
            {
                goalWorldState = DetermineGoalState(candidateGoals);
                candidateGoals.Remove(goalWorldState);
                actionPlan = GOAPPlanner.Plan(this, availableActions, currentWorldState, goalWorldState);
            }

            if (actionPlan == null)
            {
                Debug.Log("No goal achievable for the character! Deactivating the character for a while.");
                active = false;
                Invoke("ReactivateCharacter", inactiveSeconds);
            }
            else
            {
                if(debugMessages) Debug.Log(PrettyPrint(actionPlan));
                KeyValuePair<GOAPAction, Transform> currentCombination = actionPlan.Dequeue();
                currentAction = currentCombination.Key;
                target = currentCombination.Value.gameObject;
                currentTargetNull = target == null;
                currentAction.EnterAction(this);
            }
        }

        public void ReactivateCharacter()
        {
            active = true;
        }

        public abstract GOAPState DetermineGoalState(HashSet<GOAPState> candidateGoals);

        public abstract GOAPState GetCurrentWorldState();

        /// <summary>
        /// Moves towards the current action's target
        /// </summary>
        public abstract void MoveAgent(float deltaTime);

        public abstract void StartMoving();

        public abstract void StopMoving();

        public GOAPAction CurrentAction
        {
            get { return currentAction; }
        }

        public float GetDistanceCost(Vector3 target)
        {
            //For now, naïve implementation, in the future this should also be checked by A*
            return Vector3.Distance(target, transform.position) * distanceCostMultiplier;
        }

        public float GetDistanceCost(Vector3 virtualAgentPosition, Vector3 target)
        {
            return Vector3.Distance(target, virtualAgentPosition) * distanceCostMultiplier;
        }

        public float ActionStartTime
        {
            get
            {
                return actionStartTime;
            }

            //set
            //{
            //    actionStartTime = value;
            //}
        }

        //public void ActionTimeTick(float deltaTime)
        //{
        //    actionTimer += deltaTime;
        //}

        public void StartTimer(float actionStartTime)
        {
            this.actionStartTime = actionStartTime;
        }

        public static string PrettyPrint(Queue<KeyValuePair<GOAPAction, Transform>> actions)
        {
            String s = "";
            foreach (KeyValuePair<GOAPAction, Transform> a in actions)
            {
                s += a.Key.GetType().Name;
                s += "-> ";
            }
            s += "GOAL";
            return s;
        }

        //NOTE: functions from here on directly added from https://github.com/sploreg/goap, 
        //which is the main information source for this work
        public static string PrettyPrint(GOAPState state)
        {
            return state.PrettyPrint();
        }

        public static string PrettyPrint(Queue<GOAPAction> actions)
        {
            String s = "";
            foreach (GOAPAction a in actions)
            {
                s += a.GetType().Name;
                s += "-> ";
            }
            s += "GOAL";
            return s;
        }

        public static string PrettyPrint(GOAPAction[] actions)
        {
            String s = "";
            foreach (GOAPAction a in actions)
            {
                s += a.GetType().Name;
                s += ", ";
            }
            return s;
        }

        public static string PrettyPrint(GOAPAction action)
        {
            String s = "" + action.GetType().Name;
            return s;
        }
    }
}
