﻿/*Made by José María Pizana, 2018
 * 
 * http://www.jmpizana.com
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace ECUtils
{

    public static class GeneralExtensions
    {
        /// <summary>
        /// Copies an object data to another by value.
        /// Returns NullReferenceException if any of its values is null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="destiny"></param>
        /// <param name="source"></param>
        public static void CopyNotNull<T>(this T destiny, T source)
        {
            FieldInfo[] fields = source.GetType().GetFields();

            foreach (FieldInfo v in fields)
            {
                var value = v.GetValue(source);

                if (value == null) throw new NullReferenceException("An IParams object was found to have null values when assigning it");

                if (!v.IsStatic)
                    v.SetValue(destiny, value);
            }
        }

        /// <summary>
        /// Copies an object data to another by value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="destiny"></param>
        /// <param name="source"></param>
        public static T CopyShallow<T>(this T destiny, T source)
        {
            FieldInfo[] fields = source.GetType().GetFields();

            foreach (FieldInfo v in fields)
            {
                var value = v.GetValue(source);

                if (!v.IsStatic)
                    v.SetValue(destiny, value);
            }
            return destiny;
        }

        //https://www.c-sharpcorner.com/UploadFile/ff2f08/deep-copy-of-object-in-C-Sharp/
        //public static T CloneObjectSerializing<T>(this T objSource)
        //{
        //    using (MemoryStream stream = new MemoryStream())
        //    {
        //        BinaryFormatter formatter = new BinaryFormatter();
        //        formatter.Serialize(stream, objSource);
        //        stream.Position = 0;
        //        return (T)formatter.Deserialize(stream);
        //    }
        //}

        //public static T CloneObject<T>(this T objSource)
        //{
        //    //Get the type of source object and create a new instance of that type
        //    Type typeSource = objSource.GetType();
        //    T objTarget = (T)Activator.CreateInstance(typeSource);

        //    //Get all the properties of source object type
        //    //PropertyInfo[] propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
        //    FieldInfo[] fields = typeSource.GetFields();

        //    //Assign all source property to taget object 's properties
        //    foreach (FieldInfo field in fields)
        //    {
        //        //check whether property type is value type, enum or string type
        //        if (field.FieldType.IsValueType || field.FieldType.IsEnum || field.FieldType.Equals(typeof(System.String)))
        //        {
        //            field.SetValue(objTarget, field.GetValue(objSource));
        //        }
        //        //else property type is object/complex types, so need to recursively call this method until the end of the tree is reached
        //        else
        //        {
        //            object objPropertyValue = field.GetValue(objSource);

        //            if (objPropertyValue == null)
        //            {
        //                field.SetValue(objTarget, null);
        //            }
        //            else
        //            {
        //                field.SetValue(objTarget, objPropertyValue.CloneObject());
        //            }
        //        }
        //    }
        //    return objTarget;
        //}

        /// <summary>
        /// Compares folders to an array of non duplicate valid names. 
        /// </summary>
        /// <param name="folders"></param>
        /// <param name="validNames"></param>
        public static bool CompareFoldersWithNamesStrict(this DirectoryInfo[] folders, string[] validNames, bool caseSensitive = false)
        {
            List<string> usedNames = new List<string>(validNames.Length);

            if (!caseSensitive)
                for (int i = 0; i < validNames.Length; i++) validNames[i] = validNames[i].ToUpper();

            if (folders.Length != validNames.Length)
            {
                Debug.Log("Folders amount is different than names amount");
                return false;
            }

            for (int i = 0; i < folders.Length; i++)
            {
                string folderName = folders[i].Name;
                if (!caseSensitive) folderName = folderName.ToUpper();

                if (usedNames.Contains(folderName))
                {
                    Debug.Log("Duplicate folder found: " + folderName);
                    return false;
                }

                if (!validNames.Contains(folderName))
                {
                    Debug.Log("Non-permitted folder name found: " + folderName);
                    return false;
                }

                usedNames.Add(folderName);
            }

            return true;
        }

        public static DirectoryInfo GetByName(this DirectoryInfo[] folders, string name, bool caseSensitive = false)
        {
            string[] folderNames = new string[folders.Length];
            folders.GetNames().CopyTo(folderNames, 0);

            if (folders == null || folders.Length == 0)
                throw new UnityException("Input folders are null or empty");

            if (!caseSensitive)
            {
                for (int i = 0; i < folderNames.Length; i++)
                {
                    folderNames[i] = folderNames[i].ToUpper();
                }
                name = name.ToUpper();
            }
            //Debug.Log(name);
            //folderNames.ForEach((string s) => Debug.Log(s));

            for (int i = 0; i < folders.Length; i++)
            {
                if (folderNames[i] == name) return folders[i];
            }

            Debug.Log("No folder found with requested name");
            return null;
        }

        public static string[] GetNames(this DirectoryInfo[] folders)
        {
            string[] names = new string[folders.Length];
            for (int i = 0; i < names.Length; i++)
            {
                names[i] = folders[i].Name;
            }

            return names;
        }
    }


}
