﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(HexMapGenerator))]
public class HexMapGeneratorEditor : Editor
{
    HexMapGenerator generator;
    bool generatedSeed = false;
    int previousSeed;
    public override void OnInspectorGUI()
    {
        generator = target as HexMapGenerator;

        if(GUILayout.Button("New seed"))
        {
            previousSeed = generator.seed;
            Undo.RecordObject(generator, "New Seed");

            generator.seed = Random.Range(0, int.MaxValue);
            generator.seed ^= (int)System.DateTime.Now.Ticks;
            generator.seed ^= (int)Time.unscaledTime;
            generator.seed &= int.MaxValue;

            EditorUtility.SetDirty(generator);
            generatedSeed = true;
        }

        if (generatedSeed)
        {
            GUILayout.Label("Previous Seed: " + previousSeed);
        }

        DrawDefaultInspector();
    }
}   
