﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(ECCellPrefabReferencesM.TypeCellWeightTriplet))]
public class CustomTripletDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        Rect test = new Rect(position.x, position.y,
            position.width / 3, position.height);
        Rect test2 = new Rect(position.x + position.width / 3, position.y,
            position.width / 3, position.height);
        Rect test3 = new Rect(position.x + 2*position.width / 3, position.y,
            position.width / 3, position.height);

        EditorGUI.PropertyField(test, property.FindPropertyRelative("first"), GUIContent.none);
        EditorGUI.PropertyField(test2, property.FindPropertyRelative("second"), GUIContent.none);
        EditorGUI.PropertyField(test3, property.FindPropertyRelative("third"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
