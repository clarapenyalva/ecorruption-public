﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(StateValuePair))]
public class StateValuePairDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        Rect test = new Rect(position.x, position.y,
            position.width / 2, position.height);
        Rect test2 = new Rect(position.x + position.width / 2, position.y,
            position.width / 2, position.height);

        EditorGUI.PropertyField(test, property.FindPropertyRelative("first"), GUIContent.none);
        EditorGUI.PropertyField(test2, property.FindPropertyRelative("second"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
