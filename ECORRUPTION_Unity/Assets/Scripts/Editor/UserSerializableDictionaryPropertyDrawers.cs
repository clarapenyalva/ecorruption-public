﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using ECUtils;

[CustomPropertyDrawer(typeof(StringStringDictionary))]
[CustomPropertyDrawer(typeof(ObjectColorDictionary))]
[CustomPropertyDrawer(typeof(ECCellPrefabReferencesM.ECCellsPrefabsDict))]
[CustomPropertyDrawer(typeof(TotalStateDict))]
public class AnySerializableDictionaryPropertyDrawer : SerializableDictionaryPropertyDrawer {}
